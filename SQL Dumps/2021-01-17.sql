-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: carfix_web_base
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `building_number` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `post_code` int NOT NULL,
  `street_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (4,'10','Kklp','LT',12345,'dsfdf pl.'),(5,'15','Kaunas','LT',55555,'Testuojama g.'),(6,'1','Klp','LV',147412,'cxvcx pl.'),(7,'4A','KLAIPED','asad',123456789,'dsfsdf pl.'),(8,'1','KLP','LT',111111,'dsfds pl.'),(9,'1','klll','kll',111115,'sdfsd g.'),(10,'10','klp','klp',11111,'sdfdsf pl.'),(12,'10','Klaipeda','LT',11111,'sdafd g.'),(13,'15','Klaipeda','USA',11111111,'dsfsdfds g.'),(14,'10','Alytus','LT',159753,'Zukausko g.');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) NOT NULL,
  `engine_displacement_m3` varchar(255) NOT NULL,
  `engine_power_kw` int NOT NULL,
  `engine_type` varchar(255) NOT NULL,
  `manufacture_year` varchar(255) NOT NULL,
  `series_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,'BMW','3.0',120,'gasoline','2005-06','E87'),(2,'BMW','3.0',120,'diesel','2005-06','E87'),(3,'BMW','3.0',120,'diesel','2005-06','E91'),(4,'BMW','3.0',120,'gasoline','2005-06','E91'),(5,'BMW','3.0',120,'gas','2005-06','E91');
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_services`
--

DROP TABLE IF EXISTS `car_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car_services` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `employees_num` bigint NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `address` bigint NOT NULL,
  `work_time` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtbj8bbs1gj3m4yyk35i7l99w3` (`address`),
  KEY `FKji91q2xu563ppxcbe80hjwjbm` (`work_time`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_services`
--

LOCK TABLES `car_services` WRITE;
/*!40000 ALTER TABLE `car_services` DISABLE KEYS */;
INSERT INTO `car_services` VALUES (4,5,'fdsfsd',4,4),(5,1,'Servisas',5,5),(7,150,'asdsadf',12,7),(8,15,'dsfsdfdssdf',13,8),(9,159,'UAB Autobilis',14,9);
/*!40000 ALTER TABLE `car_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `details` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `detail_name` varchar(255) NOT NULL,
  `detail_type` varchar(255) NOT NULL,
  `origin_country` varchar(255) NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `car_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9ntuqq2h5spxokhxrb3vqtenm` (`car_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `details`
--

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` VALUES (1,'Vairas','Vairas','Germany',1100.00,1),(2,'Stabdziu diskas','Ratai','Germany',75.35,1);
/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failures`
--

DROP TABLE IF EXISTS `failures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failures` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `failure_description` varchar(10000) NOT NULL,
  `failure_location` varchar(255) NOT NULL,
  `failure_type` varchar(255) NOT NULL,
  `user` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbwmxe4rm1xbqayb6c38wastfh` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failures`
--

LOCK TABLES `failures` WRITE;
/*!40000 ALTER TABLE `failures` DISABLE KEYS */;
INSERT INTO `failures` VALUES (1,'While rotating steering there\'s a squeaking sound from it.','Steering','Power steering',1),(2,'While rotating steering there\'s a squeaking sound from it.','Steering','Power steering',1),(3,'While rotating steering there\'s a squeaking sound from it.','Steering','Power steering',10),(5,'Neatsoka stabdziu kaladeliu supportas.','Kairysis ratas','Kairysis ratas',10),(6,'WHILE ROTATING','POWER','Power steering',1),(7,'cxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffffcxvxcsfdffffffffffffffffffff','xcvcx','xcvxcv',10),(8,'ASASDASASASASASSAS','DFD','FDF',10),(9,'dsfsdfds','dsfs','dsf',10);
/*!40000 ALTER TABLE `failures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registrations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `total_price` decimal(19,2) NOT NULL,
  `detail_id` bigint NOT NULL,
  `failure_id` bigint NOT NULL,
  `schedule_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `user_car_id` bigint NOT NULL,
  `work_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsbnbbyqtelv3ug7j26jwucaif` (`detail_id`),
  KEY `FKi49p4puitayudmuvl9ke3mb9b` (`failure_id`),
  KEY `FKhk8abkd71foaexmwctkl0p8is` (`schedule_id`),
  KEY `FKl2iby9n9hp8jwkfj8i96pkxpi` (`user_id`),
  KEY `FKb7q1rsgl40vdwxn8tidslekwo` (`user_car_id`),
  KEY `FKrw4ertvts1cjhhiubgnpyt8cg` (`work_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrations`
--

LOCK TABLES `registrations` WRITE;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
INSERT INTO `registrations` VALUES (1,1115.00,1,1,1,1,1,1);
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_USER',NULL),(2,'ROLE_MODERATOR',NULL),(3,'ROLE_ADMIN',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedules` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `free_space` bit(1) NOT NULL,
  `time` varchar(255) NOT NULL,
  `car_service_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4hebe50e2un9ym209uuyks7f4` (`car_service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,'2021-01-15',_binary '\0','11:30',9);
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cars`
--

DROP TABLE IF EXISTS `user_cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_cars` (
  `user_id` bigint NOT NULL,
  `car_id` bigint NOT NULL,
  PRIMARY KEY (`user_id`,`car_id`),
  KEY `FKe1ijlu3q6x47kmom9a4lkn1c` (`car_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cars`
--

LOCK TABLES `user_cars` WRITE;
/*!40000 ALTER TABLE `user_cars` DISABLE KEYS */;
INSERT INTO `user_cars` VALUES (1,1),(1,2),(10,4);
/*!40000 ALTER TABLE `user_cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,3),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`),
  UNIQUE KEY `UKkwds03ohobcd8p6eowkw0f5bm` (`phone_number`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'donliskas@gmail.com','Donatas','Lisauskas','$2a$10$jlYtPYLBWflCZNW2Lh4O0Ox3T8jHP9xbnzYnAkSPLL3Fx0SLgIS6u','+370-63855525','donliskas'),(2,'valantaite.asta@mail.com','Asta','Valantaite','$2a$10$Oe1nWUWXNipKTBhntsY3IOdW8vXTGYJHGPOK.YcykPDz9FkbiK4Oe','+370-65585395','astaval'),(3,'petrauskasp@gmail.com','Petras','Petrauskas','$2a$10$//oJYgES4qopsbIuYrr./erDXHv5sKDFZIeiX3CHPGWHFVopSq2bm','+370-68533396','pratraupetr'),(4,'donliskass@gmail.com','Donatas','Lisauskas','$2a$10$pl1wTudiL0jvr.6boI9tYO69K38e3h3CVw1VjvGCn5dGGVHwiZb.K','+370-63855522','donliskass'),(5,'donlisau@gmail.com','Donatas','Lisauskas','$2a$10$CDVmNHnNKob9rRESsU638OM9IJGFw85PEvOMKRtk921IsiAvZPue.','+370-63855555','donlisau'),(6,'donlis@gmail.com','Donas','Lisas','$2a$10$gAm2OhSGtNRpCeY/mU/oCOtdPIgfdE8VjIDIwA85X2hZs.zOtyHSi','+370-633333333','donlis'),(7,'rasyte@gmail.com','Rasa','Rasyte','$2a$10$CFBjP3lBanxcrZpICcMybOmoXA6LrKZNasryGUv0OYu4sE0DV4Pra','+370-651215456','rasaas'),(8,'rataitis@gmail.com','Pranas','Rataitis','$2a$10$mBWm6fDJshI8nY3zjiWDguMecE/Wg94DJeBb0WJ7.NLBsq384jZ3e','+370-63855585','pranratis'),(9,'pranatratais@gmail.com','Pranas','Rataitis','$2a$10$YQYC4GVxP0aJYUpEvuyUpeip.bXL0uT61oeoTQdOZr1NhaK6mXUe6','+370-12312312','pranatratais'),(10,'testUser@gmail.com','TestuotojasUser','TestuotojasUser','$2a$10$vJ89sAfRZOBTS1XgQMbw/eWd8fwT0ZC16seS7zCYVsqGdu6qVB3Bu','+370-99999999','testUser');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_times`
--

DROP TABLE IF EXISTS `work_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_times` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `friday` varchar(255) NOT NULL,
  `monday` varchar(255) NOT NULL,
  `saturday` varchar(255) NOT NULL,
  `sunday` varchar(255) NOT NULL,
  `thursday` varchar(255) NOT NULL,
  `tuesday` varchar(255) NOT NULL,
  `wednesday` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_times`
--

LOCK TABLES `work_times` WRITE;
/*!40000 ALTER TABLE `work_times` DISABLE KEYS */;
INSERT INTO `work_times` VALUES (4,'08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00','08:00-18:00'),(5,'01:00-01:00','01:00-02:00','01:00-01:00','01:00-01:00','01:00-01:00','01:00-01:00','01:00-02:00'),(7,'00:00-00:00','17:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00'),(8,'00:00-00:00','00:00-00:00','00:00-00:00','00:00-03:00','00:00-00:00','00:00-00:00','00:00-00:00'),(9,'00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00','00:00-00:00');
/*!40000 ALTER TABLE `work_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `works`
--

DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `works` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `price` decimal(19,2) NOT NULL,
  `work_name` varchar(255) NOT NULL,
  `work_type` varchar(255) NOT NULL,
  `car_id` bigint DEFAULT NULL,
  `car_service_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa4d8m1eiibpi1cb0db5uuqmpw` (`car_id`),
  KEY `FKqiplwqdo8l04mq09hbrxj1sev` (`car_service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `works`
--

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` VALUES (1,15.00,'Air conditioner diagnostic','Engine',1,5),(2,300.75,'Steering','Steering wheel',1,9);
/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'carfix_web_base'
--

--
-- Dumping routines for database 'carfix_web_base'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-17 10:45:54
