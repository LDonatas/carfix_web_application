package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.CarService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CarServiceRepository extends JpaRepository<CarService, Long> {

    List<CarService> findAllByServiceName(String serviceName);

    @Query(value = "SELECT cs FROM CarService cs INNER JOIN cs.address where cs.address.streetName = :address AND cs.address.buildingNumber = :buildingNumber")
    List<CarService> findByAddress(String address, String buildingNumber);

    @Query(value = "SELECT cs FROM CarService cs INNER JOIN cs.address INNER JOIN cs.workTime WHERE " +
            "cs.serviceName = :request OR " +
            "cs.address.streetName = :request OR " +
            "cs.address.postCode = :request OR " +
            "cs.address.country = :request OR " +
            "cs.address.city = :request OR " +
            "cs.workTime.saturday = :request OR " +
            "cs.workTime.sunday = :request")
    List<CarService> search(String request);

}
