package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.CarService;
import com.example.carfix.carfixspringboot.entities.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    @Query(value = "SELECT sc FROM Schedule sc INNER JOIN sc.carService WHERE sc.carService.id = :carServiceId " +
            "AND sc.date = :date AND sc.time = :time")
    Schedule findSchedule(Long carServiceId, Date date, String time);

    @Query(value = "SELECT sc FROM Schedule sc INNER JOIN sc.carService WHERE sc.carService.id = :carServiceId " +
            "AND sc.date = :date AND sc.time = :time AND sc.freeSpace = :freeSpace")
    Schedule findScheduleForUpdate(Long carServiceId, Date date, String time, Boolean freeSpace);

    @Query(value = "SELECT sc FROM Schedule sc INNER JOIN sc.carService WHERE sc.carService.id = :carServiceId " +
            "AND sc.freeSpace = true")
    List<Schedule> findSchedulesCarServiceWhereIsFreeSpace(Long carServiceId);

    @Query(value = "SELECT sc FROM Schedule sc WHERE sc.freeSpace = true")
    List<Schedule> findSchedulesWhereIsFreeSpace();

    @Query(value = "SELECT sh FROM Schedule sh INNER JOIN sh.carService INNER JOIN sh.carService.address INNER JOIN sh.carService.workTime WHERE " +
            "sh.carService.serviceName = :request OR " +
            "sh.carService.address.streetName = :request OR " +
            "sh.carService.address.postCode = :request OR " +
            "sh.carService.address.country = :request OR " +
            "sh.carService.address.city = :request OR " +
            "sh.carService.workTime.saturday = :request OR " +
            "sh.carService.workTime.sunday = :request")
    List<Schedule> search(String request);
}
