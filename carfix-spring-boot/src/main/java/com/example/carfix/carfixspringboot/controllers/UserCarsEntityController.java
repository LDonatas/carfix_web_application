package com.example.carfix.carfixspringboot.controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.payload.response.UserCarsResponse;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.payload.request.UserCarsRequest;
import com.example.carfix.carfixspringboot.payload.response.User;
import com.example.carfix.carfixspringboot.repositories.CarRepository;
import com.example.carfix.carfixspringboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/user_cars")
public class UserCarsEntityController {

    @Autowired
    CarRepository carRepository;

    @Autowired
    UserRepository userRepository;

    @PutMapping("/edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createCarEntry(@Valid @RequestBody UserCarsRequest userCarsRequest) {

        Optional<com.example.carfix.carfixspringboot.entities.User> user = userRepository.findById(userCarsRequest.getUserID());

        Optional<Car> car = carRepository.findById(userCarsRequest.getCarID());

        List<Car> foundSame = new ArrayList<>();

        if (user.isPresent() && car.isPresent()) {

            user.get().getCars().stream()
                    .filter(check -> check.getId().equals(userCarsRequest.getCarID()))
                    .forEach(foundSame::add);

            if (foundSame.isEmpty()) {

                user.get().getCars().add(car.get());
                userRepository.save(user.get());

                return ResponseEntity.ok(new MessageResponse("Your new car registered successfully!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("This car is registered as yours!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("Failed! This car or user doesn't exist!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCarServiceEntry(@Valid @RequestParam Long userID, @Valid @RequestParam Long carID) {

        Optional<com.example.carfix.carfixspringboot.entities.User> user = userRepository.findById(userID);

        Optional<Car> car = carRepository.findById(carID);

        if (user.isPresent()) {

            user.get().getCars().remove(car.get());

            userRepository.save(user.get());

            return ResponseEntity.ok(new MessageResponse("Your car " + car.get().getBrandName() + " " + car.get().getSeriesName() + " deleted successfully!"));
        } else {
            return ResponseEntity.ok(new MessageResponse("This Car doesn't exist!"));
        }
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllServiceEntries() {

        List<com.example.carfix.carfixspringboot.entities.User> users = userRepository.findAll();

        List<UserCarsResponse> userCarsResponses = new ArrayList<>();

        if (!users.isEmpty()) {
            users.stream().filter(hasCars -> !hasCars.getCars().isEmpty())
                    .forEach(i -> userCarsResponses.add(new UserCarsResponse(
                            new User(i.getId(),
                                    i.getFirstname(),
                                    i.getLastname(),
                                    i.getEmail(),
                                    i.getPhoneNumber(),
                                    i.getUsername()),
                            i.getCars())));

            return ResponseEntity.ok().body(userCarsResponses);

        } else {
            return ResponseEntity.ok(new MessageResponse("This user doesn't exist!"));
        }
    }

    @GetMapping("/getUserCarsByUserId/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchUserCarsByUserId(@PathVariable("id") Long id) {

        Optional<com.example.carfix.carfixspringboot.entities.User> user = userRepository.findById(id);

        List<UserCarsResponse> userCarsResponses = new ArrayList<>();

        if (user.isPresent()) {

            UserCarsResponse userCarsResponse = new UserCarsResponse(
                    new User(user.get().getId(),
                            user.get().getFirstname(),
                            user.get().getLastname(),
                            user.get().getEmail(),
                            user.get().getPhoneNumber(),
                            user.get().getUsername()),
                    user.get().getCars());

            if (!userCarsResponse.getCars().isEmpty()) {
                return ResponseEntity.ok().body(userCarsResponse);
            } else {
                return ResponseEntity.ok(new MessageResponse(user.get().getUsername() + " doesn't have cars!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("This user doesn't exist!"));
        }
    }

}
