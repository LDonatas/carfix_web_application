package com.example.carfix.carfixspringboot.payload.response;

import com.example.carfix.carfixspringboot.entities.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class RegistrationResponse {

    private Long id;

    private User user;

    private Car userCar;

    private Work work;

    private Detail detail;

    private Failure failure;

    private Schedule schedule;

    private BigDecimal totalPrice;

}
