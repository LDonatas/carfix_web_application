package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.payload.response.DetailResponse;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.CarRepository;
import com.example.carfix.carfixspringboot.repositories.DetailRepository;
import com.example.carfix.carfixspringboot.services.DetailEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/details")
public class DetailEntityController {

    @Autowired
    DetailRepository detailRepository;

    @Autowired
    DetailEntityService detailEntityService;

    @Autowired
    CarRepository carRepository;

    @PostMapping("edit/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createDetailEntry(@Valid @RequestBody Detail detailRequest) {

        Optional<Car> car = carRepository.findById(detailRequest.getCar().getId());

        List<Detail> checkDetailList = detailRepository.findByDetailTypeAndDetailNameAndOriginCountry(
                detailRequest.getDetailType(),
                detailRequest.getDetailName(),
                detailRequest.getOriginCountry());

        List<Detail> foundSame = new ArrayList<>();

        if (car.isPresent()) {

            checkDetailList.stream()
                    .filter(i -> i.getCar().getId().equals(car.get().getId()))
                    .filter(i -> i.getDetailType().equals(detailRequest.getDetailType()))
                    .filter(i -> i.getDetailName().equals(detailRequest.getDetailName()))
                    .filter(i -> i.getPrice().equals(detailRequest.getPrice()))
                    .filter(i -> i.getOriginCountry().equals(detailRequest.getOriginCountry()))
                    .forEach(foundSame::add);

            if (checkDetailList.isEmpty() || foundSame.isEmpty()) {

                Detail newDetailEntry = new Detail();
                newDetailEntry.setDetailType(detailRequest.getDetailType());
                newDetailEntry.setDetailName(detailRequest.getDetailName());
                newDetailEntry.setPrice(detailRequest.getPrice());
                newDetailEntry.setOriginCountry(detailRequest.getOriginCountry());
                newDetailEntry.setCar(car.get());

                detailRepository.save(newDetailEntry);

                return ResponseEntity.ok(new MessageResponse("New Detail entry is registered successfully!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("This Detail entry already exists!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not this car entry in the database by id " + detailRequest.getCar().getId() + "!"));
        }
    }

    @PutMapping("/edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateDerailEntry(@Valid @RequestBody Detail detailRequest) {

        Optional<Car> car = carRepository.findById(detailRequest.getCar().getId());
        Optional<Detail> detail = detailRepository.findById(detailRequest.getId());

        List<Detail> checkDetailList = detailRepository.findByDetailTypeAndDetailNameAndOriginCountry(
                detailRequest.getDetailType(),
                detailRequest.getDetailName(),
                detailRequest.getOriginCountry());

        List<Detail> foundSame = new ArrayList<>();

        if (car.isPresent() && detail.isPresent()) {

            checkDetailList.stream()
                    .filter(i -> i.getCar().getId().equals(car.get().getId()))
                    .filter(i -> i.getDetailType().equals(detailRequest.getDetailType()))
                    .filter(i -> i.getDetailName().equals(detailRequest.getDetailName()))
                    .filter(i -> i.getPrice().equals(detailRequest.getPrice()))
                    .filter(i -> i.getOriginCountry().equals(detailRequest.getOriginCountry()))
                    .forEach(foundSame::add);

            if (foundSame.isEmpty()) {

                detail.get().setDetailType(detailRequest.getDetailType());
                detail.get().setDetailName(detailRequest.getDetailName());
                detail.get().setPrice(detailRequest.getPrice());
                detail.get().setOriginCountry(detailRequest.getOriginCountry());
                detail.get().setCar(car.get());

                detailRepository.save(detail.get());

                return ResponseEntity.ok(new MessageResponse("Detail entry is updated successfully!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("This Detail entry already exists!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not this car entry in the database by id " + detailRequest.getCar().getId() + "!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteDetailEntry(@Valid @RequestParam Long id) {
        Optional<Detail> detail = detailRepository.findById(id);

        if (detail.isPresent()) {

            detailRepository.delete(detail.get());

            return ResponseEntity.ok(new MessageResponse("Detail entry by id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This detail doesn't exist by id: " + id + "!"));
        }
    }


    @GetMapping("/getAll")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllDetailEntries() {

        List<Detail> details = detailRepository.findAll();

        List<DetailResponse> detailResponses = new ArrayList<>();

        if (!details.isEmpty()) {
            details.stream().forEach(i -> detailResponses.add(new DetailResponse(
                    i.getId(),
                    i.getDetailType(),
                    i.getDetailName(),
                    i.getPrice(),
                    i.getOriginCountry(),
                    new Car(i.getCar().getId(),
                            i.getCar().getBrandName(),
                            i.getCar().getSeriesName(),
                            i.getCar().getManufactureYear(),
                            i.getCar().getEngineDisplacement(),
                            i.getCar().getEngineType(),
                            i.getCar().getEnginePower()))));
            return ResponseEntity.ok().body(detailResponses);
        } else {
            return ResponseEntity.ok(new MessageResponse("There are no details entries in the database at all."));
        }
    }

    @GetMapping("/getDetailById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchById(@PathVariable("id") Long id) {

        Optional<Detail> detail = detailRepository.findById(id);

        if (detail.isPresent()) {
            return ResponseEntity.ok().body(detail);
        } else {
            return ResponseEntity.ok(new MessageResponse("This Detail service doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<Detail> details = detailRepository.search(request);
        if (!details.isEmpty()) {
            return ResponseEntity.ok().body(details);
        } else {
            ResponseEntity<?> responseDetailByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName = detailEntityService.getDetailByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName(request);
            if (detailEntityService.found) {
                return responseDetailByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName;
            }

            ResponseEntity<?> responseDetailByDetailTypeAndDetailNameAndOriginCountryAndPrice = detailEntityService.getDetailByDetailTypeAndDetailNameAndOriginCountryAndPrice(request);
            if (detailEntityService.found) {
                return responseDetailByDetailTypeAndDetailNameAndOriginCountryAndPrice;
            }

            ResponseEntity<?> responseDetailByDetailTypeAndDetailNameAndPrice = detailEntityService.getDetailByDetailTypeAndDetailNameAndPrice(request);
            if (detailEntityService.found) {
                return responseDetailByDetailTypeAndDetailNameAndPrice;
            }

            ResponseEntity<?> responseDetailByDetailTypeAndDetailNameAndOriginCountry = detailEntityService.getDetailByDetailTypeAndDetailNameAndOriginCountry(request);
            if (detailEntityService.found) {
                return responseDetailByDetailTypeAndDetailNameAndOriginCountry;
            }

            ResponseEntity<?> responseDetailByCarBrandNameAndSeriesName = detailEntityService.getDetailByCarBrandNameAndSeriesName(request);
            if (detailEntityService.found) {
                return responseDetailByCarBrandNameAndSeriesName;
            }
            ResponseEntity<?> responseDetailByDetailTypeAndDetailName = detailEntityService.getDetailByDetailTypeAndDetailName(request);
            if (detailEntityService.found) {
                return responseDetailByDetailTypeAndDetailName;
            } else {
                return ResponseEntity.ok(detailEntityService.messageResponse(request));
            }
        }
    }
}
