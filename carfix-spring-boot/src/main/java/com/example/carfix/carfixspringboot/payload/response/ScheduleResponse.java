package com.example.carfix.carfixspringboot.payload.response;

import com.example.carfix.carfixspringboot.entities.CarService;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleResponse {

    private Long id;

    private String date;

    private String time;

    private CarService carService;

    private Boolean freeSpace;

}
