package com.example.carfix.carfixspringboot.controllers;

import com.example.carfix.carfixspringboot.entities.User;
import com.example.carfix.carfixspringboot.entities.roles.ERole;
import com.example.carfix.carfixspringboot.entities.roles.Role;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.RoleRepository;
import com.example.carfix.carfixspringboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/properties")
public class ContentController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    @PostMapping("/changeRoleToAdmin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> changeRoleToAdmin(@RequestParam Long userId, @RequestParam String role) {

        Optional<User> user = userRepository.findById(userId);

        if (user.isPresent()) {

            Set<Role> roles = new HashSet<>();

            switch (role) {
                case "ADMIN":
                    Role adminRole = roleRepository.findByRole(ERole.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(adminRole);
                    break;
                case "MODERATOR":
                    Role modRole = roleRepository.findByRole(ERole.ROLE_MODERATOR)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(modRole);
                    break;
                default:
                    Role userRole = roleRepository.findByRole(ERole.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(userRole);
            }

            //TODO: make custom exception class (Role accesabillity exception)

            user.get().setRoles(roles);

            userRepository.save(user.get());

            return ResponseEntity.ok(new MessageResponse("The role of user id: " + user.get().getId()
                     + ", username: "+ user.get().getUsername() + " changed into the " + roles));
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not user entries in the database!"));
        }
    }

}
