package com.example.carfix.carfixspringboot.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RegistrationRequest {

    Long registrationId;

    Long userId;

    Long userCarId;

    Long workId;

    Long detailId;

    Long failureId;

    Long scheduleId;
}
