package com.example.carfix.carfixspringboot.payload.response;

import com.example.carfix.carfixspringboot.entities.Car;
import com.example.carfix.carfixspringboot.payload.response.User;
import lombok.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class UserCarsResponse {

    private User user;

    private Set<Car> cars;

}
