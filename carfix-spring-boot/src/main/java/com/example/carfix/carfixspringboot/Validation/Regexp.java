package com.example.carfix.carfixspringboot.Validation;
public class Regexp {

   public final static String WORK_TIME = "(?:(?:2[0-3])|(?:[01]?[0-9]))(?:\\:[0-5][0-9])?-((?<=-)(?:(?:2[0-3])|(?:[01]?[0-9]))(?:\\:[0-5][0-9])?)|Not working";
   public final static String PHONE_NUMBER = "^(\\+\\d{1,3}-)?\\d{1,14}$";
   public final static String MANUFACTURE_YEARS = "[1-2][0-9][0-9][0-9]-[0][0-9]|[1-2][0-9][0-9][0-9]-[1][0-2]";
   public final static String ENGINE_TYPE = "diesel|gasoline|electric|gas";
   public final static String DOUBLE_NUMBER = "\\d+.\\d+";
   public final static String PRICE = "[0-9]+.[0-9][0-9]";
   public final static String REGISTRATION_TIME = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
   public final static String REGISTRATION_DATE = "((20[2-9][0-9]-[0][1-9]|20[2-9][0-9]-[1][0-2])-([0][1-9]|[1][0-9]|[2][0-9]|[3][0-1]))";
   public final static String STREET_REGEXP = "([A-z]+ (g.|pl.|pr.))";
   public final static String BUILDING_NUMBER = "(\\d+[A-z])|(\\d+[a-z])|\\d+";
   public final static String CITY_OR_COUNTRY = "[A-Z]+|[a-z]+|[A-z]+";
   public final static String WORK_TYPE = "Steering wheel|Wheel|Engine|Electronics|Diagnostics";


}
