package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.Car;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.CarRepository;
import com.example.carfix.carfixspringboot.services.CarEntityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/cars")
public class CarEntityController {

    private static final Logger LOGGER = LogManager.getLogger(CarEntityController.class);

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarEntityService carEntityService;

    @PostMapping("/edit/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createCarEntry(@Valid @RequestBody Car carRequest) {

        Car car = carRepository.findCarByBrandNameAndSeriesNameAndManufactureYearAndEngineDisplacementAndEngineTypeAndEnginePower(
                carRequest.getBrandName(),
                carRequest.getSeriesName(),
                carRequest.getManufactureYear(),
                carRequest.getEngineDisplacement(),
                carRequest.getEngineType(),
                carRequest.getEnginePower());

        if (car == null) {
            car = new Car();
            car.setSeriesName(carRequest.getSeriesName());
            car.setBrandName(carRequest.getBrandName());
            car.setManufactureYear(carRequest.getManufactureYear());
            car.setEngineDisplacement(carRequest.getEngineDisplacement());
            car.setEnginePower(carRequest.getEnginePower());
            car.setEngineType(carRequest.getEngineType());

            carRepository.save(car);

            LOGGER.info("New Car entry registered successfully! " + car.toString());
            return ResponseEntity.ok(new MessageResponse("New Car entry registered successfully!"));
        } else {
            LOGGER.info("This Car model already exists! " + car.toString());
            return ResponseEntity.ok(new MessageResponse("This Car model already exists!"));
        }
    }

    @PutMapping("/edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateCarEntry(@Valid @RequestBody Car carRequest) {

        Optional<Car> car = carRepository.findById(carRequest.getId());

        if (car.isPresent()) {

            car.get().setSeriesName(carRequest.getSeriesName());
            car.get().setBrandName(carRequest.getBrandName());
            car.get().setManufactureYear(carRequest.getManufactureYear());
            car.get().setEngineDisplacement(carRequest.getEngineDisplacement());
            car.get().setEnginePower(carRequest.getEnginePower());
            car.get().setEngineType(carRequest.getEngineType());

            carRepository.save(car.get());

            LOGGER.info("Car entry by id: " + carRequest.getId() + " updated successfully!");

            return ResponseEntity.ok(new MessageResponse("Car entry by id: " + carRequest.getId() + " updated successfully!"));
        } else {
            LOGGER.info("This Car model not exists! " + carRequest.toString());

            return ResponseEntity.ok(new MessageResponse("This Car model not exists!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCarEntry(@Valid @RequestParam Long id) {

        Optional<Car> car = carRepository.findById(id);

        if (car.isPresent()) {

            carRepository.delete(car.get());

            LOGGER.info("Car entry by id: " + id + " deleted successfully!");

            return ResponseEntity.ok(new MessageResponse("Car entry by id: " + id + " deleted successfully!"));

        } else {

            LOGGER.info("This Car model not exists by id: " + id + "!");

            return ResponseEntity.ok(new MessageResponse("This Car model not exists by id: " + id + "!"));
        }
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllCarEntries() {

        List<Car> carList = carRepository.findAll();

        if (!carList.isEmpty()) {

            LOGGER.info("requesting: /api/carfix/cars/getAll");

            return ResponseEntity.ok().body(carList);
        } else {

            LOGGER.info("requesting: /api/carfix/cars/getAll ->" + "There are no car entries in the database at all.");

            return ResponseEntity.ok(new MessageResponse("There are no car entries in the database at all."));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<Car> cars = carRepository.search(request);
        if (!cars.isEmpty()) {
            LOGGER.info("Returning: /api/carfix/cars/getAll by request: " + request);
            return ResponseEntity.ok().body(cars);
        } else {
            ResponseEntity<?> responseCarByAll = carEntityService.getCarByAll(request);
            if (carEntityService.found) {

                LOGGER.info("Returning: /api/carfix/cars/getAll by request: " + request);
                return responseCarByAll;
            }
            ResponseEntity<?> responseCarsByBrandNameAndSeriesNameAndEngineType = carEntityService.getCarsByBrandNameAndSeriesNameAndEngineType(request);
            if (carEntityService.found) {
                LOGGER.info("Returning: /api/carfix/cars/getAll by request: " + request);
                return responseCarsByBrandNameAndSeriesNameAndEngineType;
            }
            ResponseEntity<?> responseCarsByBrandAndSeriesName = carEntityService.getCarByBrandAndSeriesName(request);
            if (carEntityService.found) {
                LOGGER.info("Returning: /api/carfix/cars/getAll by request: " + request);
                return responseCarsByBrandAndSeriesName;
            }
            else {
                return ResponseEntity.ok(carEntityService.messageResponse(request));
            }
        }
    }

    @GetMapping("/getCarById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchById(@PathVariable("id") Long id) {

        Optional<Car> car = carRepository.findById(id);

        if (car.isPresent()) {
            LOGGER.info("Returning: /api/carfix/getCarById/" + id);
            return ResponseEntity.ok().body(car);
        } else {
            LOGGER.info("Returning: /api/carfix/getCarById/" + id + ". There aren't car entries in the database by id: " + id + "!");
            return ResponseEntity.ok(new MessageResponse("There aren't car entries in the database by id: " + id + "!"));
        }
    }
}
