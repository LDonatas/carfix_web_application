package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.Detail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Long> {

    List<Detail> findByDetailTypeAndDetailName(String detailType, String detailName);

    List<Detail> findByDetailTypeAndDetailNameAndOriginCountry(String detailType, String detailName, String originCountry);

    List<Detail> findByDetailTypeAndDetailNameAndPrice(String detailType, String detailName, BigDecimal price);

    List<Detail> findByDetailTypeAndDetailNameAndOriginCountryAndPrice(String detailType, String detailName, String originCountry, BigDecimal price);

    List<Detail> findByDetailTypeAndDetailNameAndPriceAndOriginCountry(String detailType, String detailName, BigDecimal price, String originCountry);

    @Query(value = "SELECT d FROM Detail d INNER JOIN d.car WHERE d.car.brandName = :brandName AND d.car.seriesName = :seriesName")
    List<Detail> findByCarBrandNameAndSeriesName(String brandName, String seriesName);

    @Query(value = "SELECT d FROM Detail d INNER JOIN d.car WHERE d.detailType = :detailType AND d.detailName = :detailName AND d.car.brandName = :brandName " +
            "AND d.car.seriesName = :seriesName")
    List<Detail> findByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName(String detailType, String detailName, String brandName,  String seriesName);

    @Query(value = "SELECT d FROM Detail d INNER JOIN d.car WHERE " +
            "d.detailType = :request OR " +
            "d.detailName = :request OR " +
            "d.originCountry = :request OR " +
            "d.price = :request " +
            "OR d.car.brandName = :request " +
            "OR d.car.seriesName = :request")
    List<Detail> search(String request);

}
