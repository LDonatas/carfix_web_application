package com.example.carfix.carfixspringboot.payload.request;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DetailRequest {

    @NotNull
    private String detailType;

    @NotNull
    private String detailName;

    @NotNull
    private Double price;

    @NotNull
    private String originCountry;

    @NotNull
    private Long carId;

}
