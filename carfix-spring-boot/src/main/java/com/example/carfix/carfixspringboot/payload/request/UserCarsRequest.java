package com.example.carfix.carfixspringboot.payload.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCarsRequest {

    @NonNull
    private Long userID;

    @NonNull
    private Long carID;

}
