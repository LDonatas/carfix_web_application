package com.example.carfix.carfixspringboot.payload.response;

import com.example.carfix.carfixspringboot.entities.Car;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetailResponse {

    private Long id;

    private String detailType;

    private String detailName;

    private BigDecimal price;

    private String originCountry;

    private Car car;

}
