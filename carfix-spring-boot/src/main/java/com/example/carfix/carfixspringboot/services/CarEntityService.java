package com.example.carfix.carfixspringboot.services;

import com.example.carfix.carfixspringboot.entities.Car;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.CarRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarEntityService {

    private static final Logger LOGGER = LogManager.getLogger(CarEntityService.class);

    @Autowired
    CarRepository carRepository;

    public Boolean found = false;

    private static String[] splitRequest(String request) {
        String regexp = " ";
        return request.split(regexp);
    }

    public MessageResponse messageResponse(String request) {

        LOGGER.info("Returning: /api/carfix/cars/getAll by request: " + request + ". Response: There aren't car entries in the database by: " + request + "!");

        return new MessageResponse("There aren't car entries in the database by: " + request + "!");
    }

    public ResponseEntity<?> getCarByAll(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);

            Car foundCar = carRepository.findCarByBrandNameAndSeriesNameAndManufactureYearAndEngineDisplacementAndEngineTypeAndEnginePower(
                    splits[0], splits[1], splits[2], splits[3], splits[4], Integer.valueOf(splits[5]));

            if (foundCar != null) {
                List<Car> carsToResponse = new ArrayList<>();
                carsToResponse.add(foundCar);
                found = true;
                return ResponseEntity.ok().body(carsToResponse);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getCarByBrandAndSeriesName(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Car> foundCars = carRepository.findByBrandNameAndSeriesName(splits[0], splits[1]);
            if (!foundCars.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundCars);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getCarsByBrandNameAndSeriesNameAndEngineType(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Car> foundCars = carRepository.findByBrandNameAndSeriesNameAndEngineType(splits[0], splits[1], splits[2]);
            if (!foundCars.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundCars);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }
}
