package com.example.carfix.carfixspringboot.payload.request;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ScheduleRequest {

    @NotNull
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private String time;

    @NotNull
    private Long carServiceId;

    @NotNull
    private Boolean freeSpace;
}
