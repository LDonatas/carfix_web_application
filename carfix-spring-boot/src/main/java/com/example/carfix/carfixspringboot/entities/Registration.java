package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "registrations")
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @ManyToOne
    @NotNull(message = "User input is mandatory!")
    @JoinColumn(name = "user_id")
    private User user;

    @NonNull
    @ManyToOne
    @NotNull(message = "User car input is mandatory!")
    @JoinColumn(name = "userCar_id")
    private Car userCar;

    @NonNull
    @ManyToOne
    @NotNull(message = "Work input is mandatory!")
    @JoinColumn(name = "work_id")
    private Work work;

    @NonNull
    @ManyToOne
    @NotNull(message = "Detail input is mandatory!")
    @JoinColumn(name = "detail_id")
    private Detail detail;

    @NonNull
    @ManyToOne
    @NotNull(message = "Failure input is mandatory!")
    @JoinColumn(name = "failure_id")
    private Failure failure;

    @NonNull
    @ManyToOne
    @NotNull(message = "Schedule input is mandatory!")
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    @NonNull
    @NotNull(message = "Total price is mandatory!")
    private BigDecimal totalPrice;

}
