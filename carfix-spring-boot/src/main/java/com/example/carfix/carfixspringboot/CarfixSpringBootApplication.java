package com.example.carfix.carfixspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarfixSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarfixSpringBootApplication.class, args);
    }
}
