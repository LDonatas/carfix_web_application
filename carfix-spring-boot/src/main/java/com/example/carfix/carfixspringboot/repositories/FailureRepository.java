package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.Failure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface FailureRepository extends JpaRepository<Failure, Long> {

    @Query(value = "SELECT f FROM Failure f INNER JOIN f.user WHERE f.user.id = :userId")
    List<Failure> findFailureByUserId(Long userId);

    @Query(value = "SELECT f FROM Failure f INNER JOIN f.user WHERE f.failureType = :request OR " +
            "f.failureLocation = :request OR f.user.id = :request OR f.user.username = :request OR " +
            "f.user.email = :request OR f.user.phoneNumber = :request")
    List<Failure> search(String request);

    @Query(value = "SELECT f FROM Failure f INNER JOIN f.user WHERE f.user.firstname = :firstname AND f.user.lastname = :lastname")
    List<Failure> searchByUserFirstnameAndLastname(String firstname, String lastname);

}
