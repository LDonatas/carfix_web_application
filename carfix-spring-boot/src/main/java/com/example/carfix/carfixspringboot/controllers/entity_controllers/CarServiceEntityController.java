package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.AddressRepository;
import com.example.carfix.carfixspringboot.repositories.CarServiceRepository;
import com.example.carfix.carfixspringboot.repositories.WorkTimeRepository;
import com.example.carfix.carfixspringboot.services.CarServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/carServices")
public class CarServiceEntityController {

    @Autowired
    CarServiceRepository carServiceRepository;

    @Autowired
    WorkTimeRepository workTimeRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    CarServiceService carServiceService;

    @PostMapping("edit/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createCarServiceEntry(@Valid @RequestBody CarService carServiceRequest) {

        List<CarService> checkServiceNameList = carServiceRepository.findAllByServiceName(carServiceRequest.getServiceName());

        List<CarService> foundSame = new ArrayList<>();

        Address newAddress = carServiceRequest.getAddress();

        WorkTime newWorkTime = carServiceRequest.getWorkTime();

        checkServiceNameList.stream()
                .filter(i -> i.getAddress().getStreetName().equals(carServiceRequest.getAddress().getStreetName()))
                .filter(i -> i.getAddress().getBuildingNumber().equals(carServiceRequest.getAddress().getBuildingNumber()))
                .filter(i -> i.getAddress().getCity().equals(carServiceRequest.getAddress().getCity()))
                .filter(i -> i.getAddress().getCountry().equals(carServiceRequest.getAddress().getCountry()))
                .filter(i -> i.getAddress().getPostCode().equals(carServiceRequest.getAddress().getPostCode()))
                .forEach(foundSame::add);

        if (checkServiceNameList.isEmpty() || foundSame.isEmpty()) {

            CarService newCarServiceEntry = new CarService();
            newCarServiceEntry.setServiceName(carServiceRequest.getServiceName());
            newCarServiceEntry.setEmployeesNum(carServiceRequest.getEmployeesNum());
            newCarServiceEntry.setAddress(newAddress);
            newCarServiceEntry.setWorkTime(newWorkTime);

            carServiceRepository.save(newCarServiceEntry);

            return ResponseEntity.ok(new MessageResponse("New Car service entry is registered successfully!"));

        } else {

            return ResponseEntity.ok(new MessageResponse("This Car service already exists!"));
        }
    }

    @PutMapping("/edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateCarServiceEntry(@Valid @RequestBody CarService carServiceRequest) {

        Optional<CarService> carService = carServiceRepository.findById(carServiceRequest.getId());

        if (carService.isPresent()) {
            Optional<Address> address = addressRepository.findById(carService.get().getAddress().getId());
            Optional<WorkTime> workTime = workTimeRepository.findById(carService.get().getWorkTime().getId());

            carService.get().setServiceName(carServiceRequest.getServiceName());
            carService.get().setEmployeesNum(carServiceRequest.getEmployeesNum());

            address.get().setStreetName(carServiceRequest.getAddress().getStreetName());
            address.get().setBuildingNumber(carServiceRequest.getAddress().getBuildingNumber());
            address.get().setCity(carServiceRequest.getAddress().getCity());
            address.get().setCountry(carServiceRequest.getAddress().getCountry());
            address.get().setPostCode(carServiceRequest.getAddress().getPostCode());

            workTime.get().setMonday(carServiceRequest.getWorkTime().getMonday());
            workTime.get().setTuesday(carServiceRequest.getWorkTime().getTuesday());
            workTime.get().setWednesday(carServiceRequest.getWorkTime().getWednesday());
            workTime.get().setThursday(carServiceRequest.getWorkTime().getThursday());
            workTime.get().setFriday(carServiceRequest.getWorkTime().getFriday());
            workTime.get().setSaturday(carServiceRequest.getWorkTime().getSaturday());
            workTime.get().setSunday(carServiceRequest.getWorkTime().getSunday());

            addressRepository.save(address.get());
            workTimeRepository.save(workTime.get());
            carServiceRepository.save(carService.get());

            return ResponseEntity.ok(new MessageResponse("Car service entry by id: " + carServiceRequest.getId() + " updated successfully!"));
        } else {
            return ResponseEntity.ok(new MessageResponse("This Car service doesn't exist by id: " + carServiceRequest.getId() + "!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCarServiceEntry(@Valid @RequestParam Long id) {

        Optional<CarService> carService = carServiceRepository.findById(id);

        if (carService.isPresent()) {
            Optional<Address> address = addressRepository.findById(carService.get().getAddress().getId());
            Optional<WorkTime> workTime = workTimeRepository.findById(carService.get().getWorkTime().getId());

            carServiceRepository.delete(carService.get());
            addressRepository.delete(address.get());
            workTimeRepository.delete(workTime.get());

            return ResponseEntity.ok(new MessageResponse("Car service entry by id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This Car service doesn't exist by id: " + id + "!"));
        }
    }


    @GetMapping("/getAll")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllServiceEntries() {

        List<CarService> carServices = carServiceRepository.findAll();

        if (!carServices.isEmpty()) {
            return ResponseEntity.ok().body(carServices);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't car service entries in the database!"));
        }
    }

    @GetMapping("/getCarServiceById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchById(@PathVariable("id") Long id) {

        Optional<CarService> carService = carServiceRepository.findById(id);

        if (carService.isPresent()) {
            return ResponseEntity.ok().body(carService);
        } else {
            return ResponseEntity.ok(new MessageResponse("This Car service doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<CarService> carServices = carServiceRepository.search(request);
        if (!carServices.isEmpty()) {
            return ResponseEntity.ok().body(carServices);
        } else {
            ResponseEntity<?> responseCarServiceByAddress = carServiceService.getCarServiceByAddress(request);
            if (carServiceService.found) {
                return responseCarServiceByAddress;
            }
            else {
                return ResponseEntity.ok(carServiceService.messageResponse(request));
            }
        }
    }

}
