package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.sql.Date;

import static com.example.carfix.carfixspringboot.Validation.Regexp.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table( name = "schedules" )
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Registration date is mandatory!")
    @DateTimeFormat
    private Date date;

    @NonNull
    @NotNull(message = "Registration time is mandatory!")
    @Pattern(regexp = REGISTRATION_TIME, message = "Invalid registration time format, must be Day HH:mm !")
    private String time;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "carService_id")
    private CarService carService;

    @NonNull
    @NotNull(message = "Free space is mandatory!")
    private Boolean freeSpace;

    //TODO: invoice



}
