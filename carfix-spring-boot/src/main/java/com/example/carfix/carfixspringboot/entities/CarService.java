package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table( name = "carServices")
public class CarService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Service name is mandatory!")
    private String serviceName;

    @NonNull
    @NotNull(message = "Address is mandatory!")
    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name = "address")
    private Address address;

    @NonNull
    @NotNull(message = "Work time is mandatory!")
    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name = "workTime")
    private WorkTime workTime;

    @NonNull
    @NumberFormat
    @NotNull(message = "Employee number is mandatory!")
    private Long employeesNum;

}

