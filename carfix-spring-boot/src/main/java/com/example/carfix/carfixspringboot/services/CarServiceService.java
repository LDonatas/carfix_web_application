package com.example.carfix.carfixspringboot.services;

import com.example.carfix.carfixspringboot.entities.Car;
import com.example.carfix.carfixspringboot.entities.CarService;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.CarServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceService {

    @Autowired
    CarServiceRepository carServiceRepository;

    public Boolean found = false;

    private static String[] splitRequest(String request) {
        String regexp = " ";
        return request.split(regexp);
    }

    public MessageResponse messageResponse(String request) {
        return new MessageResponse("There aren't car service entries in the database by: " + request + "!");
    }

    public ResponseEntity<?> getCarServiceByAddress(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<CarService> foundServices = carServiceRepository.findByAddress(splits[0] + " " + splits[1], splits[2]);

            if (!foundServices.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundServices);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }
}
