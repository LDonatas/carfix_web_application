package com.example.carfix.carfixspringboot.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarServiceResponse {

    private Long id;

    private String serviceName;

    private AddressResponse address;

    private WorkTimeResponse workTime;

    private Long employeesNum;

}
