package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.FailureRepository;
import com.example.carfix.carfixspringboot.repositories.UserRepository;
import com.example.carfix.carfixspringboot.services.FailureEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/failures")
public class FailureEntityController {

    @Autowired
    FailureRepository failureRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FailureEntityService failureEntityService;

    @PostMapping("edit/create")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createFailureEntry(@Valid @RequestBody Failure failureRequest) {

        Optional<User> user = userRepository.findById(failureRequest.getUser().getId());

        if (user.isPresent()) {

            Failure newFailure = new Failure();

            newFailure.setFailureLocation(failureRequest.getFailureLocation());
            newFailure.setFailureType(failureRequest.getFailureType());
            newFailure.setFailureDescription(failureRequest.getFailureDescription());
            newFailure.setUser(user.get());

            failureRepository.save(newFailure);

            return ResponseEntity.ok(new MessageResponse("The new failure entry is registered successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("User doesn't exist by id: " + failureRequest.getUser().getId() + "!"));
        }
    }

    @PutMapping("/edit/update")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateFailureEntry(@Valid @RequestBody Failure failureRequest) {

        Optional<User> user = userRepository.findById(failureRequest.getUser().getId());

        if (user.isPresent()) {

            Optional<Failure> failure = failureRepository.findById(failureRequest.getId());

            if (failure.isPresent()) {
                failure.get().setFailureLocation(failureRequest.getFailureLocation());
                failure.get().setFailureType(failureRequest.getFailureType());
                failure.get().setFailureDescription(failureRequest.getFailureDescription());
                failure.get().setUser(failureRequest.getUser());

                failureRepository.save(failure.get());

                return ResponseEntity.ok(new MessageResponse("Failure entry is updated successfully!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("Failure doesn't exist by id: " + failureRequest.getId() + "!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("User doesn't exist by id: " + failureRequest.getUser().getId() + "!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteFailureEntry(@Valid @RequestParam Long id) {
        Optional<Failure> failure = failureRepository.findById(id);

        if (failure.isPresent()) {

            failureRepository.delete(failure.get());

            return ResponseEntity.ok(new MessageResponse("Failure entry by id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This failure doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllFailuresEntries() {

        List<Failure> failures = failureRepository.findAll();

        if (!failures.isEmpty()) {
            return ResponseEntity.ok().body(failures);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't failure entries in the database!"));
        }
    }

    @GetMapping("/getFailureById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByFailureId(@PathVariable("id") Long id) {

        Optional<Failure> failure = failureRepository.findById(id);

        if (failure.isPresent()) {
            return ResponseEntity.ok().body(failure.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not failure entry in the database by failure id: " + id + "!"));
        }
    }

    @GetMapping("/getFailuresByUserId/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByUserId(@PathVariable("id") Long id) {

        List<Failure> failures = failureRepository.findFailureByUserId(id);

        if (!failures.isEmpty()) {
            return ResponseEntity.ok().body(failures);
        } else {
            return ResponseEntity.ok(new MessageResponse("This failure doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<Failure> failures = failureRepository.search(request);
        if (!failures.isEmpty()) {
            return ResponseEntity.ok().body(failures);
        } else {
            ResponseEntity<?> responseFailuresByUserFirstnameAndLastname = failureEntityService.getFailureByUserFirstnameAndLastName(request);
            if (failureEntityService.found) {
                return responseFailuresByUserFirstnameAndLastname;
            }
            else {
                return ResponseEntity.ok(failureEntityService.messageResponse(request));
            }
        }
    }
}
