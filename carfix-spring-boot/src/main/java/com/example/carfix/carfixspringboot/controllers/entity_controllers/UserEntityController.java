package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.User;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/users")
public class UserEntityController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllUserEntries() {

        List<User> users = userRepository.findAll();

        if (!users.isEmpty()) {
            return ResponseEntity.ok().body(users);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't user entries in the database!"));
        }
    }

    @GetMapping("/getUserById/{id}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchById(@PathVariable("id") Long id) {

        Optional<User> user = userRepository.findById(id);

        if (user.isPresent()) {
            return ResponseEntity.ok().body(user.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not user entry in the database by id: " + id + "!"));
        }
    }

    @GetMapping("/getUserByUsername/{username}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByUsername(@PathVariable("username") String username) {

        Optional<User> user = userRepository.findByUsername(username);

        if (user.isPresent()) {
            return ResponseEntity.ok().body(user.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not user entry in the database by username: " + username + "!"));
        }
    }

    @GetMapping("/getUserByEmail/{email}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByEmail(@PathVariable("email") String email) {

        Optional<User> user = userRepository.findByEmail(email);

        if (user.isPresent()) {
            return ResponseEntity.ok().body(user.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not user entry in the database by email: " + email + "!"));
        }
    }

    @GetMapping("/getUserByPhoneNumber/{phonenumber}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByPhoneNumber(@PathVariable("id") String phonenumber) {

        Optional<User> user = userRepository.findByPhoneNumber(phonenumber);

        if (user.isPresent()) {
            return ResponseEntity.ok().body(user.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not user entry in the database by phone number: " + phonenumber + "!"));
        }
    }


}
