package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table( name = "failures" )
public class Failure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Failure location is mandatory!")
    private String failureLocation;

    @NonNull
    @NotNull(message = "Failure type is mandatory!")
    private String failureType;

    @NonNull
    @Size(max = 10000)
    @NotNull(message = "Failure description is mandatory!")
    private String failureDescription;

    @NonNull
    @NotNull(message = "User is mandatory!")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user")
    private User user;

}
