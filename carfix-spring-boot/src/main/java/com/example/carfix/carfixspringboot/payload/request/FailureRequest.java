package com.example.carfix.carfixspringboot.payload.request;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FailureRequest {

    @NotNull
    private String failureLocation;

    @NotNull
    private String failureType;

    @NotNull
    private String failureDescription;

    @NotNull
    private Long userId;

}
