package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.WorkTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

public interface WorkTimeRepository extends JpaRepository<WorkTime, Long> {
}
