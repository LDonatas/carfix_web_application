package com.example.carfix.carfixspringboot.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponse {

    private Long id;

    private String streetName;

    private String buildingNumber;

    private String city;

    private String country;

    private Integer postCode;

}
