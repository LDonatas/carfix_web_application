package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

import static com.example.carfix.carfixspringboot.Validation.Regexp.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "details")
public class Detail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Detail type is mandatory!")
    private String detailType;

    @NonNull
    @NotNull(message = "Detail name is mandatory!")
    private String detailName;

    @NonNull
    @NotNull(message = "A price of detail is mandatory!")
    private BigDecimal price;

    @NonNull
    @NotNull(message = "Origin Country is mandatory!")
    @Pattern(regexp = CITY_OR_COUNTRY, message = "Invalid Origin Country name, must be plain text!")
    private String originCountry;

    @NonNull
    @NotNull(message = "Car is mandatory!")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id")
    private Car car;

}
