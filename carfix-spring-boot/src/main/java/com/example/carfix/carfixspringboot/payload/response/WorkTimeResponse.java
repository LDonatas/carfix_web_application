package com.example.carfix.carfixspringboot.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkTimeResponse {

    @NonNull
    private Long id;

    @NonNull
    private String monday;

    @NonNull
    private String tuesday;

    @NonNull
    private String wednesday;

    @NonNull
    private String thursday;

    @NonNull
    private String friday;

    @NonNull
    private String saturday;

    @NonNull
    private String sunday;

}
