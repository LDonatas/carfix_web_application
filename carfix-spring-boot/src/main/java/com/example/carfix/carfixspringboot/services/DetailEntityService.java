package com.example.carfix.carfixspringboot.services;

import com.example.carfix.carfixspringboot.entities.Detail;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.DetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class DetailEntityService {

    @Autowired
    DetailRepository detailRepository;

    public Boolean found = false;

    private static String[] splitRequest(String request) {
        String regexp = " ";
        return request.split(regexp);
    }

    public MessageResponse messageResponse(String request) {
        return new MessageResponse("There aren't detail service entries in the database by: " + request + "!");
    }

    public ResponseEntity<?> getDetailByDetailTypeAndDetailName(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails = detailRepository.findByDetailTypeAndDetailName(splits[0], splits[1]);

            if (!foundDetails.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundDetails);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getDetailByDetailTypeAndDetailNameAndOriginCountry(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails = detailRepository.findByDetailTypeAndDetailNameAndOriginCountry(splits[0], splits[1], splits[2]);

            if (!foundDetails.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundDetails);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getDetailByDetailTypeAndDetailNameAndPrice(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails = detailRepository.findByDetailTypeAndDetailNameAndPrice(splits[0], splits[1], BigDecimal.valueOf(Long.parseLong(splits[2])));

            if (!foundDetails.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundDetails);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getDetailByDetailTypeAndDetailNameAndOriginCountryAndPrice(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails1 = detailRepository.findByDetailTypeAndDetailNameAndOriginCountryAndPrice(splits[0], splits[1], splits[2], BigDecimal.valueOf(Long.parseLong(splits[3])));
            List<Detail> foundDetails2 = detailRepository.findByDetailTypeAndDetailNameAndPriceAndOriginCountry(splits[0], splits[1], BigDecimal.valueOf(Long.parseLong(splits[2])), splits[3]);

            if (!foundDetails1.isEmpty() || !foundDetails2.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(!foundDetails1.isEmpty() ? foundDetails1 : foundDetails2);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getDetailByCarBrandNameAndSeriesName(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails = detailRepository.findByCarBrandNameAndSeriesName(splits[0], splits[1]);

            if (!foundDetails.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundDetails);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }

    public ResponseEntity<?> getDetailByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Detail> foundDetails = detailRepository.findByDetailTypeAndDetailNameAndCarBrandNameAndSeriesName(splits[0], splits[1], splits[2], splits[3]);

            if (!foundDetails.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundDetails);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }
}
