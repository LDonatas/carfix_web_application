package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/schedules")
public class ScheduleEntityController {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    CarServiceRepository carServiceRepository;

    @PostMapping("edit/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createScheduleEntry(@Valid @RequestBody Schedule scheduleRequest) {

        Optional<CarService> carService = carServiceRepository.findById(scheduleRequest.getCarService().getId());

        Schedule checkSchedule = scheduleRepository.findSchedule(scheduleRequest.getCarService().getId(),
                scheduleRequest.getDate(), scheduleRequest.getTime());

        if (carService.isPresent()) {

            if (checkSchedule == null) {

                checkSchedule = new Schedule();
                checkSchedule.setDate(scheduleRequest.getDate());
                checkSchedule.setTime(scheduleRequest.getTime());
                checkSchedule.setCarService(carService.get());
                checkSchedule.setFreeSpace(scheduleRequest.getFreeSpace());

                scheduleRepository.save(checkSchedule);

                return ResponseEntity.ok(new MessageResponse("You registered the new Schedule successfully!"));

            } else {

                return ResponseEntity.ok(new MessageResponse("This schedule already exists!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not this car service entry in the database by id " + scheduleRequest.getCarService().getId() + "!"));
        }
    }

    @PutMapping("edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateScheduleEntry(@Valid @RequestBody Schedule scheduleRequest) {

        Optional<CarService> carService = carServiceRepository.findById(scheduleRequest.getCarService().getId());
        Optional<Schedule> schedule = scheduleRepository.findById(scheduleRequest.getId());

        Schedule checkSchedule = scheduleRepository.findScheduleForUpdate(scheduleRequest.getCarService().getId(),
                scheduleRequest.getDate(), scheduleRequest.getTime(), scheduleRequest.getFreeSpace());

        if (carService.isPresent() && schedule.isPresent()) {

            if (checkSchedule == null) {

                schedule.get().setDate(scheduleRequest.getDate());
                schedule.get().setTime(scheduleRequest.getTime());
                schedule.get().setCarService(carService.get());
                schedule.get().setFreeSpace(scheduleRequest.getFreeSpace());

                scheduleRepository.save(schedule.get());

                return ResponseEntity.ok(new MessageResponse("You updated the schedule successfully!"));

            } else {
                return ResponseEntity.ok(new MessageResponse("This schedule already exists!"));
            }
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not this car service entry in the database by id " + scheduleRequest.getCarService().getId() + "!"));
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteScheduleEntry(@Valid @RequestParam Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);

        if (schedule.isPresent()) {

            scheduleRepository.delete(schedule.get());

            return ResponseEntity.ok(new MessageResponse("Schedule entry by id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This schedule doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getScheduleById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByScheduleId(@PathVariable("id") Long id) {

        Optional<Schedule> schedule = scheduleRepository.findById(id);

        if (schedule.isPresent()) {
            return ResponseEntity.ok().body(schedule.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("This schedule doesn't exist by id: " + id + "!"));
        }
    }


    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllSchedulesEntries() {

        List<Schedule> schedules = scheduleRepository.findAll();

        if (!schedules.isEmpty()) {
            return ResponseEntity.ok().body(schedules);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't schedules entries in the database!"));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<Schedule> schedules = scheduleRepository.search(request);
        if (!schedules.isEmpty()) {
            return ResponseEntity.ok().body(schedules);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't schedules entries in the database by " + request + "!"));
        }
    }
}
