package com.example.carfix.carfixspringboot.services;

import com.example.carfix.carfixspringboot.entities.Registration;
import com.example.carfix.carfixspringboot.payload.response.RegistrationResponse;
import com.example.carfix.carfixspringboot.payload.response.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegistrationService {

   private User userResponse;

    public ResponseEntity<?> getRegistrationResponseEntities(List<Registration> registrationsRequest) {

        List<RegistrationResponse> registrationResponses = new ArrayList<>();

        registrationsRequest.forEach(i -> {
            userResponse = new User(
                    i.getUser().getId(),
                    i.getUser().getFirstname(),
                    i.getUser().getLastname(),
                    i.getUser().getEmail(),
                    i.getUser().getPhoneNumber(),
                    i.getUser().getUsername());

            RegistrationResponse registrationResponse = new RegistrationResponse(
                    i.getId(),
                    userResponse,
                    i.getUserCar(),
                    i.getWork(),
                    i.getDetail(),
                    i.getFailure(),
                    i.getSchedule(),
                    i.getTotalPrice());

            registrationResponses.add(registrationResponse);});

        return ResponseEntity.ok().body(registrationResponses);
    }

    public ResponseEntity<?> getRegistrationResponseEntity(Registration registrationRequest) {

        userResponse = new User(
                registrationRequest.getUser().getId(),
                registrationRequest.getUser().getFirstname(),
                registrationRequest.getUser().getLastname(),
                registrationRequest.getUser().getEmail(),
                registrationRequest.getUser().getPhoneNumber(),
                registrationRequest.getUser().getUsername());

            RegistrationResponse registrationResponse = new RegistrationResponse(
                    registrationRequest.getId(),
                    userResponse,
                    registrationRequest.getUserCar(),
                    registrationRequest.getWork(),
                    registrationRequest.getDetail(),
                    registrationRequest.getFailure(),
                    registrationRequest.getSchedule(),
                    registrationRequest.getTotalPrice());

        return ResponseEntity.ok().body(registrationResponse);
    }


}
