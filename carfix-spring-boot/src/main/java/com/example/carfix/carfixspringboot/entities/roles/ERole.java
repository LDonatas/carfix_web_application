package com.example.carfix.carfixspringboot.entities.roles;

public enum ERole {

    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN

}
