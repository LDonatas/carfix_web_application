package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.example.carfix.carfixspringboot.Validation.Regexp.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Street name is mandatory!")
    @Pattern(regexp = STREET_REGEXP, message = "Invalid street name format, must be 'street name' g. or pr. or pl.!")
    private String streetName;

    @NonNull
    @NotNull(message = "Building number is mandatory!")
    @Pattern(regexp = BUILDING_NUMBER, message = "Invalid building number, must be as example 10 or 10A!")
    private String buildingNumber;

    @NonNull
    @NotNull(message = "City is mandatory!")
    @Pattern(regexp = CITY_OR_COUNTRY, message = "Invalid city name, must be plain text!")
    private String city;

    @NonNull
    @NotNull(message = "Country is mandatory!")
    @Pattern(regexp = CITY_OR_COUNTRY, message = "Invalid country name, must be plain text!")
    private String country;

    @NonNull
    @NotNull(message = "Post code is mandatory!")
    @NumberFormat
    private Integer postCode;

}
