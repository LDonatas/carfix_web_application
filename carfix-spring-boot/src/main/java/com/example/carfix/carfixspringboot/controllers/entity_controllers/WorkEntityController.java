package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.Car;
import com.example.carfix.carfixspringboot.entities.CarService;
import com.example.carfix.carfixspringboot.entities.Schedule;
import com.example.carfix.carfixspringboot.entities.Work;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.CarRepository;
import com.example.carfix.carfixspringboot.repositories.CarServiceRepository;
import com.example.carfix.carfixspringboot.repositories.WorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/works")
public class WorkEntityController {

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarServiceRepository carServiceRepository;

    @Autowired
    WorkRepository workRepository;

    @PostMapping("edit/create")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createWorkEntry(@Valid @RequestBody Work workRequest) {

        Optional<CarService> carService = carServiceRepository.findById(workRequest.getCarService().getId());

        Optional<Car> car = carRepository.findById(workRequest.getCar().getId());

        Work checkWork = workRepository.searchBy(
                workRequest.getCar().getId(),
                workRequest.getCarService().getId(),
                workRequest.getWorkType(),
                workRequest.getWorkName(),
                workRequest.getPrice());

        if (carService.isPresent() && car.isPresent()) {

            if (checkWork == null) {

                checkWork = new Work();
                checkWork.setWorkType(workRequest.getWorkType());
                checkWork.setWorkName(workRequest.getWorkName());
                checkWork.setPrice(workRequest.getPrice());
                checkWork.setCar(car.get());
                checkWork.setCarService(carService.get());

                workRepository.save(checkWork);

                return ResponseEntity.ok(new MessageResponse("The new work entry is registered successfully!"));

            } else {
                return ResponseEntity.ok(new MessageResponse("This work entry already exists!"));
            }
        } else {
            if (carService.isEmpty()) {
                return ResponseEntity.ok(new MessageResponse("There is not this car service entry in the database!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("There is not this car entry in the database by id " + workRequest.getCar().getId() + "!"));
            }
        }
    }

    @PutMapping("edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateWorkEntry(@Valid @RequestBody Work workRequest) {

        Optional<CarService> carService = carServiceRepository.findById(workRequest.getCarService().getId());

        Optional<Car> car = carRepository.findById(workRequest.getCar().getId());

        Optional<Work> work = workRepository.findById(workRequest.getId());

        Work checkWork = workRepository.searchBy(
                workRequest.getCar().getId(),
                workRequest.getCarService().getId(),
                workRequest.getWorkType(),
                workRequest.getWorkName(),
                workRequest.getPrice());

        if (carService.isPresent() && car.isPresent()) {

            if (checkWork == null) {

                work.get().setWorkType(workRequest.getWorkType());
                work.get().setWorkName(workRequest.getWorkName());
                work.get().setPrice(workRequest.getPrice());
                work.get().setCar(car.get());
                work.get().setCarService(carService.get());

                workRepository.save(work.get());

                return ResponseEntity.ok(new MessageResponse("The work entry is updated successfully!"));

            } else {
                return ResponseEntity.ok(new MessageResponse("This work entry already exists!"));
            }
        } else {
            if (carService.isEmpty()) {
                return ResponseEntity.ok(new MessageResponse("There is not this car service entry in the database!"));
            } else {
                return ResponseEntity.ok(new MessageResponse("There is not this car entry in the database by id " + workRequest.getCar().getId() + "!"));
            }
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteWorkEntry(@Valid @RequestParam Long id) {
        Optional<Work> work = workRepository.findById(id);

        if (work.isPresent()) {

            workRepository.delete(work.get());

            return ResponseEntity.ok(new MessageResponse("Work entry by id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This work doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getWorkById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByWorkId(@PathVariable("id") Long id) {

        Optional<Work> work = workRepository.findById(id);

        if (work.isPresent()) {
            return ResponseEntity.ok().body(work.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("This work doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllWorksEntries() {

        List<Work> works = workRepository.findAll();

        if (!works.isEmpty()) {
            return ResponseEntity.ok().body(works);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't works entries in the database!"));
        }
    }

    @GetMapping("/getByAny")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByAny(@RequestParam String request) {

        List<Work> works = workRepository.search(request);
        if (!works.isEmpty()) {
            return ResponseEntity.ok().body(works);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't works entries in the database by " + request + "!"));
        }
    }

}
