package com.example.carfix.carfixspringboot.controllers.entity_controllers;

import com.example.carfix.carfixspringboot.entities.*;
import com.example.carfix.carfixspringboot.entities.User;
import com.example.carfix.carfixspringboot.payload.request.RegistrationRequest;
import com.example.carfix.carfixspringboot.payload.response.*;
import com.example.carfix.carfixspringboot.repositories.*;
import com.example.carfix.carfixspringboot.services.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/carfix/registration")
public class RegistrationEntityController {

    @Autowired
    RegistrationRepository registrationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    WorkRepository workRepository;

    @Autowired
    DetailRepository detailRepository;

    @Autowired
    FailureRepository failureRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    RegistrationService registrationService;


    @PostMapping("edit/create")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> createRegistrationEntry(@Valid @RequestBody RegistrationRequest registrationRequest) {

        Optional<User> user = userRepository.findById(registrationRequest.getUserId());
        Optional<Car> userCar = carRepository.findById(registrationRequest.getUserCarId());
        Optional<Work> work = workRepository.findById(registrationRequest.getWorkId());
        Optional<Detail> detail = detailRepository.findById(registrationRequest.getDetailId());
        Optional<Failure> failure = failureRepository.findById(registrationRequest.getFailureId());
        Optional<Schedule> schedule = scheduleRepository.findById(registrationRequest.getScheduleId());
        Registration checkRegistration = registrationRepository.findRegistration(
                registrationRequest.getUserId(),
                registrationRequest.getUserCarId(),
                registrationRequest.getWorkId(),
                registrationRequest.getDetailId(),
                registrationRequest.getFailureId(),
                registrationRequest.getScheduleId());

        if (user.isPresent() && userCar.isPresent() && work.isPresent() && detail.isPresent() && failure.isPresent() && schedule.isPresent()) {

            if (checkRegistration == null && schedule.get().getFreeSpace()
                    && detail.get().getCar().getId().equals(work.get().getCar().getId())
                    && work.get().getCar().getId().equals(userCar.get().getId())) {

                BigDecimal workPrice = new BigDecimal(String.valueOf(work.get().getPrice()));
                BigDecimal detailPrice = new BigDecimal(String.valueOf(detail.get().getPrice()));

                BigDecimal totalPrice = workPrice.add(detailPrice);

                checkRegistration = new Registration();
                checkRegistration.setUser(user.get());
                checkRegistration.setUserCar(userCar.get());
                checkRegistration.setWork(work.get());
                checkRegistration.setDetail(detail.get());
                checkRegistration.setFailure(failure.get());
                checkRegistration.setSchedule(schedule.get());
                checkRegistration.setTotalPrice(totalPrice);

                schedule.get().setFreeSpace(false);

                registrationRepository.save(checkRegistration);

                return ResponseEntity.ok(new MessageResponse("You are registered successfully!"));

            } else return getResponseEntity(userCar, work, detail, schedule);
        } else {
            return getResponseEntity(registrationRequest, user, userCar, work, detail, failure);
        }
    }

    @PutMapping("edit/update")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateRegistrationEntry(@Valid @RequestBody RegistrationRequest registrationRequest) {

        Optional<Registration> registration = registrationRepository.findById(registrationRequest.getRegistrationId());
        Optional<User> user = userRepository.findById(registrationRequest.getUserId());
        Optional<Car> userCar = carRepository.findById(registrationRequest.getUserCarId());
        Optional<Work> work = workRepository.findById(registrationRequest.getWorkId());
        Optional<Detail> detail = detailRepository.findById(registrationRequest.getDetailId());
        Optional<Failure> failure = failureRepository.findById(registrationRequest.getFailureId());
        Optional<Schedule> schedule = scheduleRepository.findById(registrationRequest.getScheduleId());
        Registration checkRegistration = registrationRepository.findRegistration(
                registrationRequest.getUserId(),
                registrationRequest.getUserCarId(),
                registrationRequest.getWorkId(),
                registrationRequest.getDetailId(),
                registrationRequest.getFailureId(),
                registrationRequest.getScheduleId());

        if (registration.isPresent() && user.isPresent() &&
                userCar.isPresent() && work.isPresent() && detail.isPresent()
                && failure.isPresent() && schedule.isPresent()) {

            //Todo: add additional boolean function (this)

            if (checkRegistration == null && schedule.get().getFreeSpace()
                    && detail.get().getCar().getId().equals(work.get().getCar().getId())
                    && work.get().getCar().getId().equals(userCar.get().getId())) {

                BigDecimal workPrice = new BigDecimal(String.valueOf(work.get().getPrice()));
                BigDecimal detailPrice = new BigDecimal(String.valueOf(detail.get().getPrice()));

                BigDecimal totalPrice = workPrice.add(detailPrice);

                registration.get().setUser(user.get());
                registration.get().setUserCar(userCar.get());
                registration.get().setWork(work.get());
                registration.get().setDetail(detail.get());
                registration.get().setFailure(failure.get());
                registration.get().setSchedule(schedule.get());
                registration.get().setTotalPrice(totalPrice);

                schedule.get().setFreeSpace(false);

                registrationRepository.save(checkRegistration);


                //Todo: add additional boolean function
                return ResponseEntity.ok(new MessageResponse("You updated your registration successfully!"));

            } else {
                return getResponseEntity(userCar, work, detail, schedule);
            }
        } else {
            return getResponseEntity(registrationRequest, user, userCar, work, detail, failure);
        }
    }

    @DeleteMapping("/edit/delete")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteRegistrationEntry(@Valid @RequestParam Long id) {
        Optional<Registration> registration = registrationRepository.findById(id);

        if (registration.isPresent()) {

            registrationRepository.delete(registration.get());

            return ResponseEntity.ok(new MessageResponse("Registration entry by user name" + registration.get().getUser().getUsername() + " and id: " + id + " deleted successfully!"));

        } else {
            return ResponseEntity.ok(new MessageResponse("This registration doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getRegistrationById/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> searchByWorkId(@PathVariable("id") Long id) {

        Optional<Registration> registration = registrationRepository.findById(id);

        if (registration.isPresent()) {
            return ResponseEntity.ok().body(registration.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("This registration doesn't exist by id: " + id + "!"));
        }
    }

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllRegistrations() {

        List<Registration> registrations = registrationRepository.findAll();
        if (!registrations.isEmpty()) {
            return registrationService.getRegistrationResponseEntities(registrations);
        } else {
            return ResponseEntity.ok(new MessageResponse("There are no registration entries in the database at all."));
        }
    }

    @GetMapping("/getAllByUserId")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> getAllByUserId(@RequestParam Long userId) {

        List<Registration> registrations = registrationRepository.findRegistrationByUserId(userId);
        if (!registrations.isEmpty()) {
            return registrationService.getRegistrationResponseEntities(registrations);
        } else {
            return ResponseEntity.ok(new MessageResponse("There aren't registration entries in the database by user id: " + userId + "!"));
        }
    }


    private ResponseEntity<?> getResponseEntity(@RequestBody @Valid RegistrationRequest registrationRequest, Optional<User> user, Optional<Car> userCar, Optional<Work> work, Optional<Detail> detail, Optional<Failure> failure) {
        if (user.isEmpty()) {
            return ResponseEntity.ok(new MessageResponse("There is not this user entry in the database by id " + registrationRequest.getUserId() + "!"));
        } else if (work.isEmpty()) {
            return ResponseEntity.ok(new MessageResponse("There is not this user car entry in the database by id " + registrationRequest.getUserCarId() + "!"));
        } else if (userCar.isEmpty()) {
            return ResponseEntity.ok(new MessageResponse("There is not this work entry in the database by id " + registrationRequest.getWorkId() + "!"));
        } else if (detail.isEmpty()) {
            return ResponseEntity.ok(new MessageResponse("There is not this detail entry in the database by id " + registrationRequest.getDetailId() + "!"));
        } else if (failure.isEmpty()) {
            return ResponseEntity.ok(new MessageResponse("There is not this failure entry in the database by id " + registrationRequest.getFailureId() + "!"));
        } else {
            return ResponseEntity.ok(new MessageResponse("There is not this schedule entry in the database by id " + registrationRequest.getScheduleId() + "!"));
        }
    }

    private ResponseEntity<?> getResponseEntity(Optional<Car> userCar, Optional<Work> work, Optional<Detail> detail, Optional<Schedule> schedule) {
        if (!schedule.get().getFreeSpace()) {
            return ResponseEntity.ok(new MessageResponse("There isn't free space at the " + schedule.get().getCarService().getServiceName() + " car service!"));
        } else if (!detail.get().getCar().getId().equals(work.get().getCar().getId())) {
            return ResponseEntity.ok(new MessageResponse("Car of Detail and your car must be the same!"));
        } else if (!work.get().getCar().getId().equals(userCar.get().getId())) {
            return ResponseEntity.ok(new MessageResponse("Car of Works and your car must be the same!"));
        } else {
            return ResponseEntity.ok(new MessageResponse("This registration already exists!"));
        }
    }
}
