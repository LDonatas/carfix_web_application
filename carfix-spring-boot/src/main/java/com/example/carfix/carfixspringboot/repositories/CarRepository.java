package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    List<Car> findByBrandNameAndSeriesName(String brandName, String seriesName);

    List<Car> findByBrandNameAndSeriesNameAndEngineType(String brandName, String seriesName, String engineType);

    Car findCarByBrandNameAndSeriesNameAndManufactureYearAndEngineDisplacementAndEngineTypeAndEnginePower(String brandName,
                                                                                                                    String seriesName,
                                                                                                                    String manufactureYear,
                                                                                                                    String engineDisplacement,
                                                                                                                    String engineType,
                                                                                                                    Integer enginePower);
    @Query(value = "SELECT c FROM Car c WHERE " +
            "c.brandName = :request OR " +
            "c.seriesName = :request OR " +
            "c.manufactureYear = :request OR " +
            "c.engineDisplacement = :request OR " +
            "c.engineType = :request OR " +
            "c.enginePower = :request")
    List<Car> search(String request);

}
