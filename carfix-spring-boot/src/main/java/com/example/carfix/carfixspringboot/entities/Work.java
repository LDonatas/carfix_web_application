package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

import static com.example.carfix.carfixspringboot.Validation.Regexp.WORK_TYPE;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table( name = "works" )
public class Work {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Work type is mandatory!")
    @Pattern(regexp = WORK_TYPE, message = "Invalid work type, must be Steering wheel or Wheel or Engine or Electronics or Diagnostics!")
    private String workType;

    @NonNull
    @NotNull(message = "Work name is mandatory!")
    private String workName;

    @NonNull
    @NotNull(message = "A price of works is mandatory!")
    private BigDecimal price;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "carService_id")
    private CarService carService;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

}
