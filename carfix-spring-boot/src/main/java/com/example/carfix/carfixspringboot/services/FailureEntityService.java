package com.example.carfix.carfixspringboot.services;

import com.example.carfix.carfixspringboot.entities.Failure;
import com.example.carfix.carfixspringboot.payload.response.MessageResponse;
import com.example.carfix.carfixspringboot.repositories.FailureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class FailureEntityService {

    @Autowired
    FailureRepository failureRepository;

    public Boolean found = false;

    private static String[] splitRequest(String request) {
        String regexp = " ";
        return request.split(regexp);
    }

    public MessageResponse messageResponse(String request) {
        return new MessageResponse("There aren't failure entries in the database by: " + request + "!");
    }

    public ResponseEntity<?> getFailureByUserFirstnameAndLastName(String request) {
        found = false;
        try {
            String[] splits = splitRequest(request);
            List<Failure> foundFailures = failureRepository.searchByUserFirstnameAndLastname(splits[0], splits[1]);

            if (!foundFailures.isEmpty()) {
                found = true;
                return ResponseEntity.ok().body(foundFailures);
            } else {
                return ResponseEntity.ok(messageResponse(request));
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            return ResponseEntity.ok(messageResponse(request));
        }
    }
}
