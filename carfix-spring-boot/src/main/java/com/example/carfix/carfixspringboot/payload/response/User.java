package com.example.carfix.carfixspringboot.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Long id;

    private String firstname;

    private String lastname;

    private String email;

    private String phoneNumber;

    private String username;

}
