package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import static com.example.carfix.carfixspringboot.Validation.Regexp.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotBlank(message = "Brand name is mandatory!")
    @NotNull
    private String brandName;

    @NonNull
    @NotBlank(message = "Series name is mandatory!")
    @NotNull
    private String seriesName;

    @NonNull
    @NotBlank(message = "Manufacture years are mandatory!")
    @NotNull
    @Pattern(regexp = MANUFACTURE_YEARS, message = "Invalid date format, must be YYYY-MM")
    private String manufactureYear;

    @NonNull
    @NotBlank(message = "Engine displacement is mandatory!")
    @NotNull
    @Pattern(regexp = DOUBLE_NUMBER, message = "Invalid displacement format, must be for example 2.0 or 1.9 in/m3")
    @Column(name = "engineDisplacement_m3")
    private String engineDisplacement;

    @NonNull
    @NotBlank(message = "Engine type is mandatory!")
    @NotNull
    @Pattern(regexp = ENGINE_TYPE, message = "Invalid engine type, must be one of  diesel or gasoline or electric or gas !")
    private String engineType;

    @NonNull
    @NotNull(message = "Engine power is mandatory!")
    @NumberFormat
    @Column(name = "enginePower_KW")
    private Integer enginePower;

}
