package com.example.carfix.carfixspringboot.entities;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.example.carfix.carfixspringboot.Validation.Regexp.WORK_TIME;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "workTimes")
public class WorkTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotNull(message = "Monday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String monday;

    @NonNull
    @NotNull(message = "Tuesday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String tuesday;

    @NonNull
    @NotNull(message = "Wednesday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String wednesday;

    @NonNull
    @NotNull(message = "Thursday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String thursday;

    @NonNull
    @NotNull(message = "Friday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String friday;

    @NonNull
    @NotNull(message = "Saturday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String saturday;

    @NonNull
    @NotNull(message = "Sunday entry is mandatory!")
    @Pattern(regexp = WORK_TIME, message = "Invalid time format, must be HH:mm-HH:mm")
    private String sunday;

}
