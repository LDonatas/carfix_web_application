package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface WorkRepository extends JpaRepository<Work, Long> {

    @Query(value = "SELECT w FROM Work w INNER JOIN w.car INNER JOIN w.carService WHERE " +
            "w.car.id = :carId " +
            "AND w.carService.id = :carServiceId " +
            "AND w.workType = :workType " +
            "AND w.workName = :workName " +
            "AND w.price = :price")
    Work searchBy(Long carId, Long carServiceId, String workType,String workName, BigDecimal price);

    @Query(value = "SELECT w FROM Work w INNER JOIN w.carService INNER JOIN w.car WHERE w.workType = :request OR " +
            "w.workName = :request OR w.price = :request OR w.car.brandName = :request OR w.car.seriesName = :request OR " +
            "w.carService.serviceName = :request OR w.carService.address.streetName = :request OR w.carService.address.country = :request OR " +
            "w.carService.address.city = :request")
    List<Work> search(String request);
}
