package com.example.carfix.carfixspringboot.repositories;

import com.example.carfix.carfixspringboot.entities.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {

    @Query(value = "SELECT r FROM Registration r INNER JOIN r.user " +
            "INNER JOIN r.work INNER JOIN r.detail " +
            "INNER JOIN r.failure INNER JOIN r.schedule " +
            "WHERE r.user.id = :userId " +
            "AND r.userCar.id = :userCarId " +
            "AND r.work.id = :workId " +
            "AND r.detail.id = :detailId " +
            "AND r.failure.id = :failureId " +
            "AND r.schedule.id = :scheduleId")
    Registration findRegistration(Long userId, Long userCarId, Long workId, Long detailId, Long failureId, Long scheduleId);

    @Query(value = "SELECT r FROM Registration r INNER JOIN r.user "+
            "WHERE r.user.id = :userId")
    List<Registration> findRegistrationByUserId(Long userId);

    @Query(value = "SELECT r FROM Registration r INNER JOIN r.schedule "+
            "WHERE r.schedule.date = :date")
    List<Registration> findAllByScheduleDate(String date);



}
