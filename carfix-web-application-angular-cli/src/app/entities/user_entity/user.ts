export class User {

  id: number;

  firstname: string;

  lastname: string;

  email: string;

  phoneNumber: string;

  username: string;

  password: string;

}
