import {CarService} from '../car_service_entity/car-service';

export class Schedule {

  id: number;

  date: string;

  time: string;

  carService: CarService;

  freeSpace: boolean;

}
