import {Car} from '../car_entity/car';

export class Detail {

  id: number;

  detailType: string;

  detailName: string;

  price: number;

  originCountry: string;

  car: Car;

}
