export class Address {

  id: number;

  streetName: string;

  buildingNumber: string;

  city: string;

  country: string;

  postCode: number;

}
