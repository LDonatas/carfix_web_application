export class RegistrationRequest {

  registrationId: number;

  userId: number;

  userCarId: number;

  workId: number;

  detailId: number;

  failureId: number;

  scheduleId: number;

}
