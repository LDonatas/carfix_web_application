import {User} from '../user_entity/user';
import {Car} from '../car_entity/car';
import {Work} from '../works_entity/work';
import {Detail} from '../detail_entity/detail';
import {Failure} from '../failure_entity/failure';
import {Schedule} from '../schedule_entity/schedule';

export class RegistrationResponse {

  id: number;

  user: User;

  userCar: Car;

  work: Work;

  detail: Detail;

  failure: Failure;

  schedule: Schedule;

  totalPrice: number;

}
