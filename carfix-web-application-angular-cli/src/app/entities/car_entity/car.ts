export class Car {

  id: number;

  brandName: string;

  seriesName: string;

  manufactureYear: string;

  engineDisplacement: string;

  engineType: string;

  enginePower: number;
}
