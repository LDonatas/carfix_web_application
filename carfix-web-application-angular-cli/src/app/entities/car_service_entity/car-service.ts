import {WorkTime} from '../work_time_entity/work-time';
import {Address} from '../address_entity/address';

export class CarService {

  id: number;

  serviceName: string;

  address: Address;

  workTime: WorkTime;

  employeesNum: number;

}
