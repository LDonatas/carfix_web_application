export class UserRegistration {

  firstname: string;

  lastname: string;

  email: string;

  phoneNumber: string;

  username: string;

  password: string;

}
