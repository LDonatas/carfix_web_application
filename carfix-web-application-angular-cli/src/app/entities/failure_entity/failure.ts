import {User} from '../user_entity/user';

export class Failure {

  id: number;

  failureLocation: string;

  failureType: string;

  failureDescription: string;

  user: User;

}
