import {Car} from '../car_entity/car';
import {User} from '../user_entity/user';

export class UserCarResponse {

  user: User;
  cars: Car[];

}
