import {CarService} from '../car_service_entity/car-service';
import {Car} from '../car_entity/car';

export class Work {

  id: number;

  workType: string;

  workName: string;

  price: number;

  carService: CarService;

  car: Car;

}
