import {Component, OnInit, TemplateRef} from '@angular/core';
import {TokenService} from "../../../services/token/token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserCarResponse} from "../../../entities/user_car_entity/user-car-response";
import {ServiceUserCar} from "../../../services/entity_services/user_car_entity_service/service-user-car.service";
import {Car} from "../../../entities/car_entity/car";
import {HttpParams} from "@angular/common/http";
import {ServiceCar} from "../../../services/entity_services/car_entity_service/service-car.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {UserCarRequest} from "../../../entities/user_car_entity/user-car-request";

@Component({
  selector: 'app-user-car',
  templateUrl: './user-car.component.html',
  styleUrls: ['./user-car.component.css']
})
export class UserCarComponent implements OnInit {

  isLoggedIn = false;
  roles: string[];
  userCar: UserCarResponse;
  carsList: Car[];
  selectedCar: Car;
  newUserCar: UserCarRequest;
  userID: number;
  carID: number;
  responseMessage = '';
  errorMessage = '';
  modalRefForCar: BsModalRef;
  modalRefForDelete: BsModalRef;
  request: string;
  carsResponseMessage = '';
  carsErrorMessage = '';
  confirmText = 'Add';

  constructor(private serviceToken: TokenService,
              private serviceUserCar: ServiceUserCar,
              private serviceCar: ServiceCar,
              private router: Router,
              private serviceModal: BsModalService,) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.serviceToken.getToken();
    if (this.isLoggedIn) {
        this.userID = this.serviceToken.getUser().id;
        this.serviceUserCar.getUserCarsWithUserId(this.userID).subscribe(
          response => { this.userCar = response;
                        this.responseMessage = response.message; },
          error => this.errorMessage = error.error.message);

    } else {
      this.navigateToLoginPage();
    }
  }

  searchCarByAnyRequest(): void {
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceCar.getByAnyRequest(params).subscribe(data => {
      this.carsList = data;
      this.carsResponseMessage = data.message;
    }, error => this.carsErrorMessage = error.error.message);
  }

  deleteUserCar(carID: number): void {
    this.modalRefForDelete.hide();
    const params = new HttpParams()
      .set('userID', String(this.userID))
      .set('carID', String(carID));
    this.serviceUserCar.deleteUserCarEntry(params).subscribe(data =>
      this.responseMessage = data.message, error => this.errorMessage = error.error.message);
    setTimeout(() => {
      this.router.navigate(['user_cars'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  selectCar(selectedCar: Car): void {
    this.modalRefForCar.hide();
    this.newUserCar = new UserCarRequest();
    this.selectedCar = selectedCar;
    this.newUserCar.userID = this.userID;
    this.newUserCar.carID = this.selectedCar.id;
    this.serviceUserCar.updateUserCarEntry(this.newUserCar).subscribe(
      data => this.responseMessage = data.message,
        error => this.errorMessage = error.error.message);
    setTimeout(() => {
      this.router.navigate(['user_cars'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  openModalForCar(template: TemplateRef<any>): void {
    this.modalRefForCar = this.serviceModal.show(template, {class: 'modal-lg'});
    this.serviceCar.getAllCarEntries().subscribe(
      carsResponse => this.carsList = carsResponse,
      error => this.carsErrorMessage = error.error.message);
  }

  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.serviceModal.show(template);
  }




}
