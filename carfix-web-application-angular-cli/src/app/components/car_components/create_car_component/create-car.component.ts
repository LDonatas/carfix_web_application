import { Component, OnInit } from '@angular/core';
import {TokenService} from '../../../services/token/token.service';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Car} from '../../../entities/car_entity/car';
import {BsDatepickerConfig, BsDatepickerViewMode} from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-create-car',
  templateUrl: './create-car.component.html',
  styleUrls: ['./create-car.component.css']
})
export class CreateCarComponent implements OnInit {

  car: Car = new Car();
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  createCarForm: FormGroup;
  submitted = false;
  isFailed = false;
  errorMessage = '';
  responseMessage = '';
  selectedDate = '';
  minMode: BsDatepickerViewMode = 'month';
  bsConfig: Partial<BsDatepickerConfig>;

  constructor(private tokenService: TokenService,
              private carService: ServiceCar,
              private router: Router) {}

  ngOnInit(): void {
    this.initDatePicker();
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn ) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.createCarForm = this.carService.getCreateCarForm();
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.createCarForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.createCarForm.invalid) {
      return;
    } else {
      this.carService.bindValues(this.car, this.createCarForm, this.selectedDate);
      this.create();
      this.isFailed = false;
      this.errorMessage = '';
      this.navigateToPage();
    }
  }

  create(): void {
    this.carService.createCarEntry(this.car)
      .subscribe(data => this.responseMessage = data.message, error => {this.errorMessage = error.error.message; this.isFailed = true; });
    this.car = new Car();
  }

  initDatePicker(): void {
    this.bsConfig = Object.assign({}, {
      minMode : this.minMode
    });
  }

  navigateToPage(): void {
    setTimeout(() => {
      this.router.navigate(['cars'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['cars']);
  }
}
