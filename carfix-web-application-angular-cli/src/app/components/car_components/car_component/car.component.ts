import {Component, OnInit, TemplateRef} from '@angular/core';
import {TokenService} from '../../../services/token/token.service';
import {Car} from '../../../entities/car_entity/car';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  cars: Car[];
  roles: string[];
  errorMessage = '';
  responseMessage = '';
  request: string;
  modalRef: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private carService: ServiceCar,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn ) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.carService.getAllCarEntries().subscribe(cars => this.cars = cars, error => this.errorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.responseMessage = '';
    this.errorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carService.getByAnyRequest(params).subscribe(data => {
      this.cars = data;
      this.responseMessage = data.message;
    }, error => this.errorMessage = error.error.message);
  }

  deleteCarEntry(deleteCar: Car): void {
    this.responseMessage = '';
    this.errorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteCar.id));
    this.carService.deleteCarEntry(params).subscribe(data => {
      this.responseMessage = data.message;
    }, error => this.errorMessage = error.error.message);
    this.modalRef.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToUpdateCar(id: number): void {
    this.router.navigate(['updateCar/' + id]);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToCarsCreate(): void {
    this.router.navigate(['cars/create']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

}
