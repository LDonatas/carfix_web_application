import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ServiceCar} from '../../../../services/entity_services/car_entity_service/service-car.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Car} from '../../../../entities/car_entity/car';
import {TokenService} from '../../../../services/token/token.service';
import {BsDatepickerConfig, BsDatepickerViewMode} from 'ngx-bootstrap/datepicker';


@Component({
  selector: 'app-car-update',
  templateUrl: './car-update.component.html',
  styleUrls: ['./car-update.component.css']
})
export class CarUpdateComponent implements OnInit {

  car: Car;
  roles: string[];
  id: number;
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  updateCarForm: FormGroup;
  submitted = false;
  isFailed = false;
  errorMessage = '';
  responseMessage = '';
  selectedDate = '';
  minMode: BsDatepickerViewMode = 'month';
  bsConfig: Partial<BsDatepickerConfig>;

  constructor(private tokenService: TokenService,
              private activatedRoute: ActivatedRoute,
              private carService: ServiceCar,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.initDatePicker();
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.id = +this.activatedRoute.snapshot.paramMap.get('id');
        this.carService.getCarWithId(this.id).subscribe(car => this.car = car,
          error => this.errorMessage = error.error.message);
        this.updateCarForm = this.carService.getUpdateCarForm();
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  initDatePicker(): void {
    this.bsConfig = Object.assign({}, {
      minMode : this.minMode
    });
  }

  get f(): { [p: string]: AbstractControl } {
    return this.updateCarForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.updateCarForm.invalid) {
      return;
    } else {
      this.carService.bindValues(this.car, this.updateCarForm, this.selectedDate);
      this.updateCar();
      this.isFailed = false;
      this.errorMessage = '';
      this.navigateToPage();
    }
  }

  updateCar(): void {
    this.carService.updateCarEntry(this.car)
      .subscribe(data => this.responseMessage = data.message, error => {
        this.errorMessage = error.error.message;
        this.isFailed = true;
      });
    this.car = new Car();
  }

  navigateToPage(): void {
    setTimeout(() => {
      this.router.navigate(['cars'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['cars']);
  }
}
