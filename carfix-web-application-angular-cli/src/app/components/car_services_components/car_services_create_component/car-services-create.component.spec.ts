import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CarServicesCreateComponent } from './car-services-create.component';

describe('CarServicesCreateComponent', () => {
  let component: CarServicesCreateComponent;
  let fixture: ComponentFixture<CarServicesCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarServicesCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarServicesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
