import {Component, OnInit, TemplateRef} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {Time} from '../../../entities/work_time_entity/week_entity/time';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-car-services-create',
  templateUrl: './car-services-create.component.html',
  styleUrls: ['./car-services-create.component.css']
})
export class CarServicesCreateComponent implements OnInit {

  carService: CarService = new CarService();
  address: Address = new Address();
  workTime: WorkTime = new WorkTime();
  time: Time = new Time();
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  createCarServiceForm: FormGroup;
  submitted = false;
  isFailed = false;
  errorMessage = '';
  responseMessage = '';
  PM = false;
  modalRef: BsModalRef;
  workTimeSaved = false;
  workTimeSavedResponse = '';

  constructor(private tokenService: TokenService,
              private carServiceService: ServiceCarService,
              private router: Router,
              private modalService: BsModalService,) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.createCarServiceForm = this.carServiceService.getCarServiceFormToCreate();
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.createCarServiceForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.createCarServiceForm.invalid) {
      return;
    } else {
      this.carServiceService.bindValues(this.carService, this.address, this.workTime, this.time, this.createCarServiceForm);
      this.create();
      this.isFailed = false;
      this.errorMessage = '';
      this.navigateToPage();
    }
  }

  create(): void {
    this.carService.address = this.address;
    this.carService.workTime = this.workTime;
    this.carServiceService.createCarServiceEntry(this.carService)
      .subscribe(response => this.responseMessage = response.message,
        error => {
          this.errorMessage = error.error.message;
          this.isFailed = true;
        });
    this.carService = new CarService();
    this.address = new Address();
    this.workTime = new WorkTime();
  }

  navigateToPage(): void {
    setTimeout(() => {
      this.router.navigate(['carServices'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['carServices']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  saveWorkTimePopover(): void {
    this.workTimeSaved = false;
    if (this.time.mondayStart !== undefined && this.time.mondayEnd !== undefined
      && this.time.tuesdayStart !== undefined && this.time.tuesdayEnd !== undefined
      && this.time.wednesdayStart !== undefined && this.time.wednesdayEnd !== undefined
      && this.time.thursdayStart !== undefined && this.time.thursdayEnd !== undefined
      && this.time.fridayStart !== undefined && this.time.fridayEnd !== undefined
      && this.time.saturdayStart !== undefined && this.time.saturdayEnd !== undefined
      && this.time.sundayStart !== undefined && this.time.sundayEnd !== undefined) {
      this.workTimeSaved = true;
      this.modalRef.hide();
    } else {
      this.workTimeSaved = false;
      this.workTimeSavedResponse = 'In order to save all the values must be filled!';
    }
  }
}
