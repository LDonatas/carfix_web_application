import {Component, OnInit, TemplateRef} from '@angular/core';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-car-services',
  templateUrl: './car-services.component.html',
  styleUrls: ['./car-services.component.css']
})
export class CarServicesComponent implements OnInit {

  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  carServices: CarService[];
  roles: string[];
  errorMessage = '';
  responseMessage = '';
  request: string;
  modalRef: BsModalRef;
  modalRefForDelete: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private carServiceService: ServiceCarService,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.carServiceService.getAllCarServiceEntries().subscribe(
          response => this.carServices = response, error => this.errorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.responseMessage = '';
    this.errorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carServiceService.getByAnyRequest(params).subscribe(data => {
      this.carServices = data;
      this.responseMessage = data.message;
    }, error => this.errorMessage = error.error.message);
  }

  deleteCarServiceEntry(deleteCarServiceEntry: CarService): void {
    this.responseMessage = '';
    this.errorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteCarServiceEntry.id));
    this.carServiceService.deleteCarServiceEntry(params).subscribe(response => {
      this.responseMessage = response.message;
    }, error => this.errorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToUpdateCarService(id: number): void {
    this.router.navigate(['updateCarService/' + id]);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToCreateCarService(): void {
    this.router.navigate(['carService/create']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }

}
