import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarServicesUpdateComponent } from './car-services-update.component';

describe('CarServicesUpdateComponent', () => {
  let component: CarServicesUpdateComponent;
  let fixture: ComponentFixture<CarServicesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarServicesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarServicesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
