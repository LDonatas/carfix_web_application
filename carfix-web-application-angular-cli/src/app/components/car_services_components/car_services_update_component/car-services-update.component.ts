import {Component, OnInit, TemplateRef} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {TokenService} from '../../../services/token/token.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {Time} from '../../../entities/work_time_entity/week_entity/time';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-car-services-update',
  templateUrl: './car-services-update.component.html',
  styleUrls: ['./car-services-update.component.css']
})
export class CarServicesUpdateComponent implements OnInit {

  carService: CarService;
  address: Address;
  workTime: WorkTime;
  time: Time = new Time();
  roles: string[];
  id: number;
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  updateCarServiceForm: FormGroup;
  submitted = false;
  isFailed = false;
  errorMessage = '';
  responseMessage = '';
  selectedDate = '';
  PM = false;
  modalRef: BsModalRef;
  workTimeSaved = false;

  constructor(private tokenService: TokenService,
              private activatedRoute: ActivatedRoute,
              private carServiceService: ServiceCarService,
              private router: Router,
              private formBuilder: FormBuilder,
              private modalService: BsModalService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.id = +this.activatedRoute.snapshot.paramMap.get('id');
        this.carServiceService.getCarServiceWithId(this.id).subscribe(carServiceResponse => {
            this.carService = carServiceResponse;
            this.address = carServiceResponse.address;
            this.workTime = carServiceResponse.workTime;
          },
          error => this.errorMessage = error.error.message);
        this.updateCarServiceForm = this.carServiceService.getCarServiceFormToUpdate();
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.updateCarServiceForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.updateCarServiceForm.invalid) {
      return;
    } else {
      this.carServiceService.bindValues(this.carService, this.address, this.workTime, this.time, this.updateCarServiceForm);
      this.updateCar();
      this.isFailed = false;
      this.errorMessage = '';
      this.navigateToPage();
    }
  }

  updateCar(): void {
    this.carService.address = this.address;
    this.carService.workTime = this.workTime;
    this.carServiceService.updateCarServiceEntry(this.carService)
      .subscribe(data => this.responseMessage = data.message, error => {
        this.errorMessage = error.error.message;
        this.isFailed = true;
      });
  }

  navigateToPage(): void {
    setTimeout(() => {
      this.router.navigate(['carServices'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['carServices']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  saveWorkTimePopover(): void {
    this.workTimeSaved = true;
    this.modalRef.hide();
  }

}
