import {Component, OnInit, TemplateRef} from '@angular/core';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Car} from '../../../entities/car_entity/car';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Detail} from '../../../entities/detail_entity/detail';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  details: Detail[];
  car: Car;
  roles: string[];
  detailsResponseMessage = '';
  detailsErrorMessage = '';
  carErrorMessage = '';
  request: string;
  modalRef: BsModalRef;
  modalRefForDelete: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private detailService: ServiceDetail,
              private carService: ServiceCar,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn ) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.detailService.getAllDetailEntries().subscribe(detailsResponse => this.details = detailsResponse,
            error => this.detailsErrorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.detailsResponseMessage = '';
    this.detailsErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.detailService.getByAnyRequest(params).subscribe(detailsResponse => {
      this.details = detailsResponse;
      this.detailsResponseMessage = detailsResponse.message;
    }, error => this.detailsErrorMessage = error.error.message);
  }

  deleteDetailEntry(deleteDetailRequest: Detail): void {
    this.detailsResponseMessage = '';
    this.detailsErrorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteDetailRequest.id));
    this.detailService.deleteDetailEntry(params).subscribe(data => {
      this.detailsResponseMessage = data.message;
    }, error => this.detailsErrorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToUpdateDetail(id: number): void {
    this.router.navigate(['updateDetail/' + id]);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToDetailCreate(): void {
    this.router.navigate(['detail/create']);
  }

  openModal(template: TemplateRef<any>, carId: number): void {
    this.modalRef = this.modalService.show(template);

    this.carService.getCarWithId(carId).subscribe(carResponse => this.car = carResponse,
      error => this.carErrorMessage = error.error.message);
  }

  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }
}
