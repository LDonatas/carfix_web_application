import {Component, OnInit, TemplateRef} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Car} from '../../../entities/car_entity/car';
import {HttpParams} from '@angular/common/http';
import {Detail} from '../../../entities/detail_entity/detail';

@Component({
  selector: 'app-detail-create',
  templateUrl: './detail-create.component.html',
  styleUrls: ['./detail-create.component.css']
})
export class DetailCreateComponent implements OnInit {

  detail: Detail = new Detail();
  cars: Car[];
  selectedCar: Car;
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  createDetailForm: FormGroup;
  submitted = false;
  isFailed = false;
  createResponseMessage = '';
  createErrorMessage = '';
  carResponseMessage = '';
  carErrorMessage = '';
  request: string;
  modalRef: BsModalRef;

  constructor(private tokenService: TokenService,
              private carService: ServiceCar,
              private detailService: ServiceDetail,
              private router: Router,
              private modalService: BsModalService,) {}

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn ) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.createDetailForm = this.detailService.getDetailsFormToCreate();
        this.carService.getAllCarEntries().subscribe(
          carsResponse => this.cars = carsResponse,
            error => this.carErrorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.createDetailForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.createDetailForm.invalid) {
      return;
    } else {
      this.detailService.bindValues(this.detail, this.createDetailForm);
      this.create();
      this.isFailed = false;
      this.createErrorMessage = '';
      this.navigateToDetailsPage();
    }
  }

  create(): void {
    this.detail.car = this.selectedCar;
    this.detailService.createDetailEntry(this.detail)
      .subscribe(data => this.createResponseMessage = data.message,
          error => {this.createErrorMessage = error.error.message; this.isFailed = true; });
    this.detail = new Detail();
  }

  navigateToDetailsPage(): void {
    setTimeout(() => {
      this.router.navigate(['details'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  searchCarByAnyRequest(): void {
    this.carResponseMessage = '';
    this.carErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carService.getByAnyRequest(params).subscribe(data => {
      this.cars = data;
      this.carResponseMessage = data.message;
    }, error => this.carErrorMessage = error.error.message);
  }

  selectCar(selectedCar: Car): void {
    this.selectedCar = selectedCar;
    this.modalRef.hide();
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['details']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
  }

}
