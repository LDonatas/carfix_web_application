import {Component, OnInit, TemplateRef} from '@angular/core';
import {Detail} from '../../../entities/detail_entity/detail';
import {Car} from '../../../entities/car_entity/car';
import {AbstractControl, FormGroup} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-detail-update',
  templateUrl: './detail-update.component.html',
  styleUrls: ['./detail-update.component.css']
})
export class DetailUpdateComponent implements OnInit {

  detailID: number;
  detail: Detail;
  cars: Car[];
  selectedCar: Car;
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  updateDetailForm: FormGroup;
  submitted = false;
  isFailed = false;
  updateResponseMessage = '';
  updateErrorMessage = '';
  carsResponseMessage = '';
  carsErrorMessage = '';
  carErrorMessage = '';
  detailsResponseMessage = '';
  detailsErrorMessage = '';
  request: string;
  modalRef: BsModalRef;

  constructor(private tokenService: TokenService,
              private carService: ServiceCar,
              private detailService: ServiceDetail,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: BsModalService,) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.detailID = +this.activatedRoute.snapshot.paramMap.get('id');
        this.updateDetailForm = this.detailService.getDetailFormToUpdate();
        this.detailService.getDetailWithId(this.detailID).subscribe(
          detailResponse => {
            this.detail = detailResponse;
            this.selectedCar = detailResponse.car;
          },
          error => this.detailsErrorMessage = error.error.message
        );
        this.carService.getAllCarEntries().subscribe(
          carsResponse => this.cars = carsResponse,
          error => this.carsErrorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.updateDetailForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.updateDetailForm.invalid) {
      return;
    } else {
      this.detailService.bindValues(this.detail, this.updateDetailForm);
      this.updateDetailEntry();
      this.isFailed = false;
      this.updateErrorMessage = '';
      this.navigateToDetailsPage();
    }
  }

  updateDetailEntry(): void {
    this.detail.car = this.selectedCar;
    this.detailService.updateDetailEntry(this.detail)
      .subscribe(data => this.updateResponseMessage = data.message,
        error => {
          this.updateErrorMessage = error.error.message;
          this.isFailed = true;
        });
  }

  navigateToDetailsPage(): void {
    setTimeout(() => {
      this.router.navigate(['details'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  searchCarByAnyRequest(): void {
    this.carsResponseMessage = '';
    this.carsErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carService.getByAnyRequest(params).subscribe(data => {
      this.cars = data;
      this.carsResponseMessage = data.message;
    }, error => this.carsErrorMessage = error.error.message);
  }

  selectCar(selectedID: Car): void {
    this.selectedCar = selectedID;
    this.modalRef.hide();
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['details']);
  }

  openModalCars(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
  }

  openModalCar(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

}
