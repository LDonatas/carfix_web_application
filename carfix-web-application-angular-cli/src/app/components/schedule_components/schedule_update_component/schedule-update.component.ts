import {Component, OnInit, TemplateRef} from '@angular/core';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {BsDatepickerConfig, BsDatepickerViewMode} from 'ngx-bootstrap/datepicker';
import {TokenService} from '../../../services/token/token.service';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Schedule} from '../../../entities/schedule_entity/schedule';

@Component({
  selector: 'app-schedule-update',
  templateUrl: './schedule-update.component.html',
  styleUrls: ['./schedule-update.component.css']
})
export class ScheduleUpdateComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];

  scheduleID: number;
  scheduleRequest: Schedule;
  carServices: CarService[];
  selectedFreeSpace: boolean;
  submitted = false;
  isFailed = false;
  updateResponseMessage = '';
  updateErrorMessage = '';
  carServiceResponseMessage = '';
  carServiceErrorMessage = '';
  scheduleResponseMessage = '';
  scheduleErrorMessage = '';
  selectedDate = '';
  selectedTime = '';

  request: string;
  modalRefForCarService: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  minMode: BsDatepickerViewMode = 'day';
  bsConfig: Partial<BsDatepickerConfig>;
  PM = false;

  constructor(private tokenService: TokenService,
              private scheduleService: ServiceSchedule,
              private carServiceService: ServiceCarService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.initDatePicker();
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      this.scheduleID = +this.activatedRoute.snapshot.paramMap.get('id');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.scheduleService.getScheduleWithId(this.scheduleID).subscribe(
          scheduleResponse => {
            this.scheduleRequest = scheduleResponse;
            this.selectedFreeSpace = scheduleResponse.freeSpace;
          },
          error => this.scheduleErrorMessage = error.error.message);
        this.carServiceService.getAllCarServiceEntries().subscribe(
          carServicesResponse => this.carServices = carServicesResponse,
          error => this.carServiceErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  initDatePicker(): void {
    this.bsConfig = Object.assign({}, {
      minMode: this.minMode
    });
  }

  selectCarService(selectedCarService: CarService): void {
    this.scheduleRequest.carService = selectedCarService;
    this.modalRefForCarService.hide();
  }

  onSubmit(): void {
    this.submitted = true;
    this.scheduleService.bindValues(this.scheduleRequest, this.selectedDate, this.selectedTime);
    this.update();
    this.updateErrorMessage = '';
    this.navigateToSchedulesPage();
  }

  update(): void {
    this.scheduleRequest.freeSpace = this.selectedFreeSpace;
    this.scheduleService.updateScheduleEntry(this.scheduleRequest)
      .subscribe(data => this.updateResponseMessage = data.message,
        error => {
          this.updateErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.scheduleRequest = new Schedule();
  }

  searchCarServicesByAnyRequest(): void {
    this.carServiceResponseMessage = '';
    this.carServiceErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carServiceService.getByAnyRequest(params).subscribe(carServiceResponse => {
      this.carServiceResponseMessage = carServiceResponse.message;
      this.carServices = carServiceResponse;
    }, error => this.carServiceErrorMessage = error.error.message);
  }

  navigateToSchedulesPage(): void {
    setTimeout(() => {
      this.router.navigate(['schedules'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  back(): void {
    this.router.navigate(['schedules']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  openModalCarService(template: TemplateRef<any>): void {
    this.modalRefForCarService = this.modalService.show(template, {class: 'modal-lg'});
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }
}
