import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {BsDatepickerConfig, BsDatepickerViewMode} from 'ngx-bootstrap/datepicker';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Schedule} from '../../../entities/schedule_entity/schedule';

@Component({
  selector: 'app-schedule-create',
  templateUrl: './schedule-create.component.html',
  styleUrls: ['./schedule-create.component.css']
})
export class ScheduleCreateComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  scheduleRequest: Schedule = new Schedule();
  carServices: CarService[];
  selectedCarService: CarService;
  submitted = false;
  isFailed = false;
  isValid = false;
  createResponseMessage = '';
  createErrorMessage = '';
  carServiceResponseMessage = '';
  carServiceErrorMessage = '';
  validationResponseMessage = '';
  selectedDate = '';
  selectedTime = '';
  selectedFreeSpace = false;
  request: string;
  modalRefForCarService: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  minMode: BsDatepickerViewMode = 'day';
  bsConfig: Partial<BsDatepickerConfig>;
  PM = false;

  constructor(private tokenService: TokenService,
              private scheduleService: ServiceSchedule,
              private carServiceService: ServiceCarService,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.initDatePicker();
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.carServiceService.getAllCarServiceEntries().subscribe(
          carServicesResponse => this.carServices = carServicesResponse,
          error => this.carServiceErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  initDatePicker(): void {
    this.bsConfig = Object.assign({}, {
      minMode: this.minMode
    });
  }

  selectCarService(selectedCarService: CarService): void {
    this.selectedCarService = selectedCarService;
    this.modalRefForCarService.hide();
  }

  onSubmit(): void {
    this.submitted = true;
    this.checkForm();
    this.scheduleService.bindValues(this.scheduleRequest, this.selectedDate, this.selectedTime);
    if (this.isValid) {
      this.create();
      this.createErrorMessage = '';
      this.navigateToSchedulesPage();
    }
  }

  checkForm(): void {
    if ( this.selectedDate !== '' && this.selectedTime !== '' && this.selectedCarService !== undefined) {
    this.isValid = true;
    } else {
      this.validationResponseMessage = 'All fields are required!';
      this.isValid = false;
    }
  }


  create(): void {
    this.scheduleRequest.carService = this.selectedCarService;
    this.scheduleRequest.freeSpace = this.selectedFreeSpace;
    this.scheduleService.createScheduleEntry(this.scheduleRequest)
      .subscribe(data => this.createResponseMessage = data.message,
        error => {
          this.createErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.scheduleRequest = new Schedule();
  }

  searchCarServicesByAnyRequest(): void {
    this.carServiceResponseMessage = '';
    this.carServiceErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carServiceService.getByAnyRequest(params).subscribe(carServiceResponse => {
      this.carServiceResponseMessage = carServiceResponse.message;
      this.carServices = carServiceResponse;
    }, error => this.carServiceErrorMessage = error.error.message);
  }

  navigateToSchedulesPage(): void {
    setTimeout(() => {
      this.router.navigate(['schedules'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  back(): void {
    this.router.navigate(['schedules']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  openModalCarService(template: TemplateRef<any>): void {
    this.modalRefForCarService = this.modalService.show(template, {class: 'modal-lg'});
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }
}
