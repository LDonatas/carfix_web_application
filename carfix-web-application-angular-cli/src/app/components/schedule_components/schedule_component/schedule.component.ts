import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {Schedule} from '../../../entities/schedule_entity/schedule';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  schedules: Observable<Schedule[]>;
  address: Address;
  workTime: WorkTime;
  schedulesResponseMessage = '';
  schedulesErrorMessage = '';
  request: string;
  modalRefForCarService: BsModalRef;
  modalRefForDelete: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private scheduleService: ServiceSchedule,
              private carServiceService: ServiceCarService,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.schedules = this.scheduleService.getAllScheduleEntries();
      }
      } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.schedulesResponseMessage = '';
    this.schedulesErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.schedules = this.scheduleService.getByAnyRequest(params);
  }

  deleteSchedulesEntry(deleteSchedulesRequest: Schedule): void {
    this.schedulesResponseMessage = '';
    this.schedulesErrorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteSchedulesRequest.id));
    this.scheduleService.deleteScheduleEntry(params).subscribe(data => {
      this.schedulesResponseMessage = data.message;
    }, error => this.schedulesErrorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  updateSchedule(schedule: Schedule): void {
    schedule.freeSpace = !schedule.freeSpace;
    this.scheduleService.updateScheduleEntry(schedule).subscribe(
      data => schedule = data as Schedule,
      error => console.log(error));
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToSchedulesCreate(): void {
    this.router.navigate(['schedule/create']);
  }

  navigateToSchedulesUpdate(id: number): void {
    this.router.navigate(['updateSchedule/' + id]);
  }

  openModalCarService(template: TemplateRef<any>, address: Address, workTime: WorkTime): void {
    this.modalRefForCarService = this.modalService.show(template);
    this.address = address;
    this.workTime = workTime;
  }
  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }
}
