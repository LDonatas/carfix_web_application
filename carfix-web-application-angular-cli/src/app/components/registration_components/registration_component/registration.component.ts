import {Component, OnInit, TemplateRef} from '@angular/core';
import {Schedule} from '../../../entities/schedule_entity/schedule';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {RegistrationResponse} from '../../../entities/registration_entity/registration-response';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Detail} from '../../../entities/detail_entity/detail';
import {User} from '../../../entities/user_entity/user';
import {Work} from '../../../entities/works_entity/work';
import {Car} from '../../../entities/car_entity/car';
import {Failure} from '../../../entities/failure_entity/failure';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';
import {ServiceWork} from '../../../services/entity_services/works_entity_service/service-work.service';
import {ServiceRegistration} from '../../../services/entity_services/registration_entity_service/service-registration.service';
import {RegistrationRequest} from '../../../entities/registration_entity/registration-request';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  registrations: RegistrationResponse[];
  userCar: Car;
  carService: CarService;
  detail: Detail;
  failure: Failure;
  schedule: Schedule;
  user: User;
  work: Work;
  address: Address;
  workTime: WorkTime;
  registrationResponseMessage = '';
  registrationErrorMessage = '';
  request: string;

  modalRefForRegistrationCreate: BsModalRef;
  modalRefForDelete: BsModalRef;
  modalRefForCar: BsModalRef;
  modalRefForUserCar: BsModalRef;
  modalRefForCarService: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  modalRefForDetail: BsModalRef;
  modalRefForFailure: BsModalRef;
  modalRefForSchedule: BsModalRef;
  modalRefForUser: BsModalRef;
  modalRefForWork: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private serviceRegistration: ServiceRegistration,
              private serviceCar: ServiceCar,
              private serviceCarService: ServiceCarService,
              private serviceDetail: ServiceDetail,
              private serviceFailure: ServiceFailure,
              private serviceSchedule: ServiceSchedule,
              private serviceWork: ServiceWork,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.serviceRegistration.getAllRegistrationEntries().subscribe(
          response => this.registrations = response,
          error => this.registrationErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.registrationResponseMessage = '';
    this.registrationErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceRegistration.getByAnyRequest(params).subscribe(
      response => {
        this.registrations = response;
        this.registrationResponseMessage = response.message;
      },
      error => this.registrationErrorMessage = error.error.message);
  }

  deleteRegistrationEntry(registrationResponse: RegistrationResponse): void {
    const params = new HttpParams()
      .set('id', String(registrationResponse.id));
    this.serviceRegistration.deleteRegistrationEntry(params).subscribe(data => {
      this.registrationResponseMessage = data.message;
    }, error => this.registrationErrorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToCreateRegistration(): void {
    this.router.navigate(['registration/create']);
  }

  navigateToUpdateRegistration(id: number): void {
    this.router.navigate(['updateRegistration/' + id]);
  }


  openModalForCar(template: TemplateRef<any>, car: Car): void {
    this.modalRefForCar = this.modalService.show(template);
    this.userCar = car;
  }

  openModalCarService(template: TemplateRef<any>, carService: CarService): void {
    this.modalRefForCarService = this.modalService.show(template);
    this.carService = carService;
    this.address = carService.address;
    this.workTime = carService.workTime;
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }

  openModalForDetail(template: TemplateRef<any>, detail: Detail): void {
    this.modalRefForDetail = this.modalService.show(template);
    this.detail = detail;
  }

  openModalForFailure(template: TemplateRef<any>, failure: Failure): void {
    this.modalRefForFailure = this.modalService.show(template);
    this.failure = failure;

  }

  openModalForSchedule(template: TemplateRef<any>, schedule: Schedule): void {
    this.modalRefForSchedule = this.modalService.show(template);
    this.schedule = schedule;
  }

  openModalForUser(template: TemplateRef<any>, user: User): void {
    this.modalRefForUser = this.modalService.show(template);
    this.user = user;
  }

  openModalForWork(template: TemplateRef<any>, work: Work): void {
    this.modalRefForWork = this.modalService.show(template);
    this.work = work;
  }

  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }

  openModalForRegistrationCreate(template: TemplateRef<any>): void {
    this.modalRefForRegistrationCreate = this.modalService.show(template, {class: 'modal-xl'});
  }
}
