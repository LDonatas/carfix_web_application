import {Component, OnInit, TemplateRef} from '@angular/core';
import {Car} from '../../../entities/car_entity/car';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Detail} from '../../../entities/detail_entity/detail';
import {Failure} from '../../../entities/failure_entity/failure';
import {Schedule} from '../../../entities/schedule_entity/schedule';
import {User} from '../../../entities/user_entity/user';
import {Work} from '../../../entities/works_entity/work';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceRegistration} from '../../../services/entity_services/registration_entity_service/service-registration.service';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceWork} from '../../../services/entity_services/works_entity_service/service-work.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {RegistrationRequest} from '../../../entities/registration_entity/registration-request';
import {ServiceUserCar} from '../../../services/entity_services/user_car_entity_service/service-user-car.service';
import {UserCarResponse} from "../../../entities/user_car_entity/user-car-response";

@Component({
  selector: 'app-registration-create',
  templateUrl: './registration-create.component.html',
  styleUrls: ['./registration-create.component.css']
})
export class RegistrationCreateComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  registrationRequest: RegistrationRequest = new RegistrationRequest();

  user: User;
  userID: number;

  userCars: UserCarResponse;
  car: Car;
  selectedUserCarID: number;
  userCarsResponseMessage = '';
  userCarsErrorMessage = '';

  carService: CarService;
  address: Address;
  workTime: WorkTime;

  details: Detail[];
  selectedDetailID: number;
  detailsResponseMessage = '';
  detailsErrorMessage = '';

  failures: Failure[];
  failure: Failure;
  selectedFailureID: number;
  failuresResponseMessage = '';
  failuresErrorMessage = '';

  schedules: Schedule[];
  selectedScheduleID: number;
  schedulesResponseMessage = '';
  schedulesErrorMessage = '';

  works: Work[];
  selectedWorkID: number;
  worksResponseMessage = '';
  worksErrorMessage = '';

  registrationCreateResponseMessage = '';
  registrationCreateErrorMessage = '';
  request: string;
  submitted = false;
  isFailed = false;
  isValid = false;

  modalRefForUserCars: BsModalRef;
  modalRefForCar: BsModalRef;
  modalRefForCarService: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  modalRefForDetail: BsModalRef;
  modalRefForFailures: BsModalRef;
  modalRefForFailure: BsModalRef;
  modalRefForSchedule: BsModalRef;
  modalRefForWork: BsModalRef;

  constructor(private tokenService: TokenService,
              private serviceRegistration: ServiceRegistration,
              private serviceUserCar: ServiceUserCar,
              private serviceCar: ServiceCar,
              private serviceCarService: ServiceCarService,
              private serviceDetail: ServiceDetail,
              private serviceFailure: ServiceFailure,
              private serviceSchedule: ServiceSchedule,
              private serviceWork: ServiceWork,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.userID = this.tokenService.getUser().id;
      this.serviceUserCar.getUserCarsWithUserId(this.userID).subscribe(
        response => {
          this.userCars = response;
          this.userCarsResponseMessage = response.message;
        },
        error => this.userCarsErrorMessage = error.error.message);
      this.serviceDetail.getAllDetailEntries().subscribe(
        detailsResponse => this.details = detailsResponse,
        error => this.detailsErrorMessage = error.error.message);
      this.serviceFailure.getFailureWithUserId(this.userID).subscribe(
        failuresResponse => {
          this.failures = failuresResponse;
          this.failuresResponseMessage = failuresResponse.message;
        },
        error => this.failuresErrorMessage = error.error.message);
      this.serviceSchedule.getAllScheduleEntries().subscribe(
        schedulesResponse => {
          this.schedules = schedulesResponse;
          /*this.carService = schedulesResponse.carService;*/
        },
        error => this.schedulesErrorMessage = error.error.message);
      this.serviceWork.getAllWorkEntries().subscribe(
        worksResponse => {
          this.works = worksResponse;
          /*this.carService = worksResponse.carService;*/
        },
        error => this.worksErrorMessage = error.error.message);
    } else {
      this.navigateToLoginPage();
    }
  }

  searchDetailByAnyRequest(): void {
    this.detailsResponseMessage = '';
    this.detailsErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceDetail.getByAnyRequest(params).subscribe(detailsResponse => {
      this.details = detailsResponse;
      this.detailsResponseMessage = detailsResponse.message;
    }, error => this.detailsErrorMessage = error.error.message);
    this.request = '';
  }

  searchFailureByAnyRequest(): void {
    this.failuresResponseMessage = '';
    this.failuresErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceFailure.getByAnyRequest(params).subscribe(failuresResponse => {
      this.failuresResponseMessage = failuresResponse.message;
      this.failures = failuresResponse;
    }, error => this.failuresErrorMessage = error.error.message);
    this.request = '';
  }

  searchScheduleByAnyRequest(): void {
    this.request = '';
    this.schedulesResponseMessage = '';
    this.schedulesErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceSchedule.getByAnyRequest(params).subscribe(
      schedulesResponse => {
        this.schedules = schedulesResponse;
        this.carService = schedulesResponse.carService;
      },
      error => this.schedulesErrorMessage = error.error.message);
    this.request = '';
  }

  searchWorksByAnyRequest(): void {
    this.worksResponseMessage = '';
    this.worksErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceWork.getByAnyRequest(params).subscribe(
      response => {
        this.works = response;
        this.worksResponseMessage = response.message;
        this.carService = response.carService;
      },
      error => this.worksErrorMessage = error.error.message);
    this.request = '';
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.isValid === true) {
      this.create();
      this.isFailed = false;
      this.navigateToRegistrationPage();
    }
  }

  create(): void {
    this.registrationRequest.userId = this.userID;
    this.registrationRequest.userCarId = this.selectedUserCarID;
    this.registrationRequest.workId = this.selectedWorkID;
    this.registrationRequest.detailId = this.selectedDetailID;
    this.registrationRequest.failureId = this.selectedFailureID;
    this.registrationRequest.scheduleId = this.selectedScheduleID;
    this.serviceRegistration.createRegistrationEntry(this.registrationRequest)
      .subscribe(data => this.registrationCreateResponseMessage = data.message,
        error => {
          this.registrationCreateErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.registrationRequest = new RegistrationRequest();
  }

  checkValid(): void {
    if (this.selectedUserCarID !== undefined
      && this.selectedDetailID !== undefined && this.selectedFailureID !== undefined
      && this.selectedScheduleID !== undefined && this.selectedWorkID !== undefined) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  selectUserCar(selectedUserCarID: number): void {
    this.selectedUserCarID = selectedUserCarID;
    this.modalRefForUserCars.hide();
    this.checkValid();
  }

  selectDetail(selectedDetailID: number): void {
    this.selectedDetailID = selectedDetailID;
    this.modalRefForDetail.hide();
    this.checkValid();
  }

  selectFailure(selectedFailureID: number): void {
    this.selectedFailureID = selectedFailureID;
    this.modalRefForFailures.hide();
    this.checkValid();
  }

  selectSchedule(selectedScheduleID: number): void {
    this.selectedScheduleID = selectedScheduleID;
    this.modalRefForSchedule.hide();
    this.checkValid();
  }

  selectWork(selectedWorkID: number): void {
    this.selectedWorkID = selectedWorkID;
    this.modalRefForWork.hide();
    this.checkValid();
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToRegistrationPage(): void {
    setTimeout(() => {
      this.router.navigate(['registrations'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToUserCars(): void {
    this.router.navigate(['user_cars']);
  }


  openModalForCars(template: TemplateRef<any>): void {
    this.modalRefForUserCars = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForCar(template: TemplateRef<any>, car: Car): void {
    this.modalRefForCar = this.modalService.show(template);
    this.car = car;
  }

  openModalForCarService(template: TemplateRef<any>, carService: CarService): void {
    this.modalRefForCarService = this.modalService.show(template);
    this.carService = carService;
    this.address = carService.address;
    this.workTime = carService.workTime;
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }

  openModalForDetail(template: TemplateRef<any>): void {
    this.modalRefForDetail = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForFailures(template: TemplateRef<any>): void {
    this.modalRefForFailures = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForFailure(template: TemplateRef<any>, failure: Failure): void {
    this.modalRefForFailure = this.modalService.show(template, {class: 'modal-xl'});
    this.failure = failure;
  }

  openModalForSchedule(template: TemplateRef<any>): void {
    this.modalRefForSchedule = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForWork(template: TemplateRef<any>): void {
    this.modalRefForWork = this.modalService.show(template, {class: 'modal-lg'});
  }

}
