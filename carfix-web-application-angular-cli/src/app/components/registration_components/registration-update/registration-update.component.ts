import {Component, OnInit, TemplateRef} from '@angular/core';
import {RegistrationRequest} from '../../../entities/registration_entity/registration-request';
import {User} from '../../../entities/user_entity/user';
import {UserCarResponse} from '../../../entities/user_car_entity/user-car-response';
import {Car} from '../../../entities/car_entity/car';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {Detail} from '../../../entities/detail_entity/detail';
import {Failure} from '../../../entities/failure_entity/failure';
import {Schedule} from '../../../entities/schedule_entity/schedule';
import {Work} from '../../../entities/works_entity/work';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceRegistration} from '../../../services/entity_services/registration_entity_service/service-registration.service';
import {ServiceUserCar} from '../../../services/entity_services/user_car_entity_service/service-user-car.service';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {ServiceDetail} from '../../../services/entity_services/detail_entity_service/service-detail.service';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';
import {ServiceSchedule} from '../../../services/entity_services/schedule_entity_service/service-schedule.service';
import {ServiceWork} from '../../../services/entity_services/works_entity_service/service-work.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {RegistrationResponse} from '../../../entities/registration_entity/registration-response';

@Component({
  selector: 'app-registration-update',
  templateUrl: './registration-update.component.html',
  styleUrls: ['./registration-update.component.css']
})
export class RegistrationUpdateComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  registrationRequest: RegistrationRequest = new RegistrationRequest();
  registrationResponse: RegistrationResponse;
  registrationID: number;
  registrationResponseMessage = '';
  registrationErrorMessage = '';

  user: User;
  userID: number;

  /*userCars: UserCarResponse;*/
  userCars: Car[];
  userCar: Car;
  selectedUserCarID: number;
  userCarsResponseMessage = '';
  userCarsErrorMessage = '';

  carService: CarService;
  address: Address;
  workTime: WorkTime;

  details: Detail[];
  detail: Detail;
  selectedDetailID: number;
  detailsResponseMessage = '';
  detailsErrorMessage = '';

  failures: Failure[];
  failure: Failure;
  selectedFailureID: number;
  failuresResponseMessage = '';
  failuresErrorMessage = '';

  schedules: Schedule[];
  schedule: Schedule;
  selectedScheduleID: number;
  schedulesResponseMessage = '';
  schedulesErrorMessage = '';

  works: Work[];
  work: Work;
  selectedWorkID: number;
  worksResponseMessage = '';
  worksErrorMessage = '';

  registrationUpdateResponseMessage = '';
  registrationUpdateErrorMessage = '';
  request: string;
  submitted = false;
  isFailed = false;

  modalRefForUserCars: BsModalRef;
  modalRefForCar: BsModalRef;
  modalRefForCarService: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  modalRefForDetail: BsModalRef;
  modalRefForFailures: BsModalRef;
  modalRefForFailure: BsModalRef;
  modalRefForSchedule: BsModalRef;
  modalRefForWork: BsModalRef;

  constructor(private serviceToken: TokenService,
              private serviceRegistration: ServiceRegistration,
              private serviceUserCar: ServiceUserCar,
              private serviceCar: ServiceCar,
              private serviceCarService: ServiceCarService,
              private serviceDetail: ServiceDetail,
              private serviceFailure: ServiceFailure,
              private serviceSchedule: ServiceSchedule,
              private serviceWork: ServiceWork,
              private router: Router,
              private modalService: BsModalService,
              private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.serviceToken.getToken();
    if (this.isLoggedIn) {
      this.roles = this.serviceToken.getUser().role;
      this.userID = this.serviceToken.getUser().id;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.registrationID = +this.activatedRoute.snapshot.paramMap.get('id');
        this.serviceRegistration.getRegistrationWithId(this.registrationID).subscribe(
          response => {
            this.registrationResponse = response;
            this.user = response.user;
            this.userCars = response.user.cars;
            this.work = response.work;
            this.detail = response.detail;
            this.failure = response.failure;
            this.schedule = response.schedule;
            this.userCar = response.userCar;
            /*this.userID = response.user.id;*/
            this.selectedUserCarID = response.userCar.id;
            this.selectedWorkID = response.work.id;
            this.selectedDetailID = response.detail.id;
            this.selectedFailureID = response.failure.id;
            this.selectedScheduleID = response.schedule.id;
          }, error => this.registrationErrorMessage = error.error.message);
        this.serviceDetail.getAllDetailEntries().subscribe(
          detailsResponse => this.details = detailsResponse,
          error => this.detailsErrorMessage = error.error.message);
        this.serviceFailure.getFailureWithUserId(this.userID).subscribe(
          failuresResponse => {
            this.failures = failuresResponse;
            this.failuresResponseMessage = failuresResponse.message;
          },
          error => this.failuresErrorMessage = error.error.message);
        this.serviceSchedule.getAllScheduleEntries().subscribe(
          schedulesResponse => {
            this.schedules = schedulesResponse;
          },
          error => this.schedulesErrorMessage = error.error.message);
        this.serviceWork.getAllWorkEntries().subscribe(
          worksResponse => {
            this.works = worksResponse;
          },
          error => this.worksErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchDetailByAnyRequest(): void {
    this.detailsResponseMessage = '';
    this.detailsErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceDetail.getByAnyRequest(params).subscribe(detailsResponse => {
      this.details = detailsResponse;
      this.detailsResponseMessage = detailsResponse.message;
    }, error => this.detailsErrorMessage = error.error.message);
    this.request = '';
  }

  searchFailureByAnyRequest(): void {
    this.failuresResponseMessage = '';
    this.failuresErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceFailure.getByAnyRequest(params).subscribe(failuresResponse => {
      this.failuresResponseMessage = failuresResponse.message;
      this.failures = failuresResponse;
    }, error => this.failuresErrorMessage = error.error.message);
    this.request = '';
  }

  searchScheduleByAnyRequest(): void {
    this.request = '';
    this.schedulesResponseMessage = '';
    this.schedulesErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceSchedule.getByAnyRequest(params).subscribe(
      schedulesResponse => {
        this.schedules = schedulesResponse;
        this.carService = schedulesResponse.carService;
      },
      error => this.schedulesErrorMessage = error.error.message);
    this.request = '';
  }

  searchWorksByAnyRequest(): void {
    this.worksResponseMessage = '';
    this.worksErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceWork.getByAnyRequest(params).subscribe(
      response => {
        this.works = response;
        this.worksResponseMessage = response.message;
        this.carService = response.carService;
      },
      error => this.worksErrorMessage = error.error.message);
    this.request = '';
  }

  onSubmit(): void {
    this.submitted = true;
    this.update();
    console.log('Registration ID ' + this.registrationRequest.registrationId);
    console.log('User ID ' + this.registrationRequest.userId);
    console.log('User car ID: ' + this.registrationResponse.userCar.id);
    console.log('Work ID ' + this.registrationResponse.work.id);
    console.log('Detail ID ' + this.registrationResponse.detail.id);
    console.log('Failure ID ' + this.registrationResponse.failure.id);
    console.log('Schedule ID ' + this.registrationResponse.schedule.id);

    this.isFailed = false;
    /*this.navigateToRegistrationPage();*/

  }

  update(): void {
    this.registrationRequest.registrationId = this.registrationID;
    this.registrationRequest.userId = this.userID;
    this.registrationRequest.userCarId = this.selectedUserCarID;
    this.registrationRequest.workId = this.selectedWorkID;
    this.registrationRequest.detailId = this.selectedDetailID;
    this.registrationRequest.failureId = this.selectedFailureID;
    this.registrationRequest.scheduleId = this.selectedScheduleID;
    this.serviceRegistration.updateRegistrationEntry(this.registrationRequest)
      .subscribe(data => this.registrationUpdateResponseMessage = data.message,
        error => {
          this.registrationUpdateErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.registrationRequest = new RegistrationRequest();
  }

  selectUserCar(selectedUserCarID: number): void {
    this.selectedUserCarID = selectedUserCarID;
    this.modalRefForUserCars.hide();
  }

  selectDetail(selectedDetailID: number): void {
    this.selectedDetailID = selectedDetailID;
    this.modalRefForDetail.hide();
  }

  selectFailure(selectedFailureID: number): void {
    this.selectedFailureID = selectedFailureID;
    this.modalRefForFailures.hide();
  }

  selectSchedule(selectedScheduleID: number): void {
    this.selectedScheduleID = selectedScheduleID;
    this.modalRefForSchedule.hide();
  }

  selectWork(selectedWorkID: number): void {
    this.selectedWorkID = selectedWorkID;
    this.modalRefForWork.hide();
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToRegistrationPage(): void {
    setTimeout(() => {
      this.router.navigate(['registrations'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToUserCars(): void {
    this.router.navigate(['user_cars']);
  }


  openModalForCars(template: TemplateRef<any>): void {
    this.modalRefForUserCars = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForCar(template: TemplateRef<any>, car: Car): void {
    this.modalRefForCar = this.modalService.show(template);
    this.userCar = car;
  }

  openModalForCarService(template: TemplateRef<any>, carService: CarService): void {
    this.modalRefForCarService = this.modalService.show(template);
    this.carService = carService;
    this.address = carService.address;
    this.workTime = carService.workTime;
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }

  openModalForDetail(template: TemplateRef<any>): void {
    this.modalRefForDetail = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForFailures(template: TemplateRef<any>): void {
    this.modalRefForFailures = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForFailure(template: TemplateRef<any>, failure: Failure): void {
    this.modalRefForFailure = this.modalService.show(template, {class: 'modal-xl'});
    this.failure = failure;
  }

  openModalForSchedule(template: TemplateRef<any>): void {
    this.modalRefForSchedule = this.modalService.show(template, {class: 'modal-xl'});
  }

  openModalForWork(template: TemplateRef<any>): void {
    this.modalRefForWork = this.modalService.show(template, {class: 'modal-xl'});
  }

}

