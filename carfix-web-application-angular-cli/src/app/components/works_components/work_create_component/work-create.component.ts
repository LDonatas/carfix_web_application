import {Component, OnInit, TemplateRef} from '@angular/core';
import {Car} from '../../../entities/car_entity/car';
import {AbstractControl, FormGroup} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Work} from '../../../entities/works_entity/work';
import {ServiceWork} from '../../../services/entity_services/works_entity_service/service-work.service';
import {ServiceCarService} from '../../../services/entity_services/car_service_entity_service/service-car.service';
import {ServiceCar} from '../../../services/entity_services/car_entity_service/service-car.service';
import {CarService} from '../../../entities/car_service_entity/car-service';

@Component({
  selector: 'app-work-create',
  templateUrl: './work-create.component.html',
  styleUrls: ['./work-create.component.css']
})
export class WorkCreateComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  work: Work = new Work();
  cars: Car[];
  selectedCar: Car;
  carServices: CarService[];
  selectedCarService: CarService;

  createWorkForm: FormGroup;
  submitted = false;
  isFailed = false;
  createResponseMessage = '';
  createErrorMessage = '';
  carResponseMessage = '';
  carErrorMessage = '';
  carServiceResponseMessage = '';
  carServiceErrorMessage = '';
  request: string;
  modalRefForCarService: BsModalRef;
  modalRefForCar: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;

  constructor(private serviceToken: TokenService,
              private serviceWork: ServiceWork,
              private serviceCarService: ServiceCarService,
              private serviceCar: ServiceCar,
              private serviceModal: BsModalService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.serviceToken.getToken();
    if (this.isLoggedIn) {
      this.roles = this.serviceToken.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.createWorkForm = this.serviceWork.getWorkFormToCreate();
        this.serviceCar.getAllCarEntries().subscribe(
          carsResponse => this.cars = carsResponse,
          error => this.carErrorMessage = error.error.message);
        this.serviceCarService.getAllCarServiceEntries().subscribe(
          carServicesResponse => this.carServices = carServicesResponse,
          error => this.carServiceErrorMessage = error.error.message);
      } else {
        this.navigateToHome();
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.createWorkForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.createWorkForm.invalid) {
      return;
    } else {
      this.serviceWork.bindValues(this.work, this.createWorkForm);
      this.create();
      this.isFailed = false;
      this.createErrorMessage = '';
      this.navigateToWorksPage();
    }
  }

  create(): void {
    this.work.car = this.selectedCar;
    this.work.carService = this.selectedCarService;
    this.serviceWork.createWorkEntry(this.work)
      .subscribe(data => this.createResponseMessage = data.message,
        error => {
          this.createErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.work = new Work();
  }

  navigateToWorksPage(): void {
    setTimeout(() => {
      this.router.navigate(['works'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  searchCarByAnyRequest(): void {
    this.carResponseMessage = '';
    this.carErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceCar.getByAnyRequest(params).subscribe(data => {
      this.cars = data;
      this.carResponseMessage = data.message;
    }, error => this.carErrorMessage = error.error.message);
  }

  searchCarServiceByAnyRequest(): void {
    this.carServiceResponseMessage = '';
    this.carServiceErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.serviceCarService.getByAnyRequest(params).subscribe(data => {
      this.carServices = data;
      this.carServiceResponseMessage = data.message;
    }, error => this.carServiceErrorMessage = error.error.message);
  }

  selectCar(selectedCar: Car): void {
    this.selectedCar = selectedCar;
    this.modalRefForCar.hide();
  }

  selectCarService(selectedCarService: CarService): void {
    this.selectedCarService = selectedCarService;
    this.modalRefForCarService.hide();
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['works']);
  }

  openModalForCar(template: TemplateRef<any>): void {
    this.modalRefForCar = this.serviceModal.show(template, {class: 'modal-xl'});
  }

  openModalCarService(template: TemplateRef<any>): void {
    this.modalRefForCarService = this.serviceModal.show(template, {class: 'modal-xl'});
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.serviceModal.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.serviceModal.show(template);
  }

}
