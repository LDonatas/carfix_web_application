import {Component, OnInit, TemplateRef} from '@angular/core';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Work} from '../../../entities/works_entity/work';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Car} from '../../../entities/car_entity/car';
import {ServiceWork} from '../../../services/entity_services/works_entity_service/service-work.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit {

  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  isLoggedIn = false;
  roles: string[];
  works: Work[];
  carService: CarService;
  car: Car;
  address: Address;
  workTime: WorkTime;
  worksResponseMessage = '';
  worksErrorMessage = '';
  request: string;
  modalRefForDelete: BsModalRef;
  modalRefForCarService: BsModalRef;
  modalRefForCar: BsModalRef;
  modalRefForAddress: BsModalRef;
  modalRefForWorkTime: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private workService: ServiceWork,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.workService.getAllWorkEntries().subscribe(response => { this.works = response; this.worksResponseMessage = response.message; },
          error => this.worksErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
        this.worksResponseMessage = '';
        this.worksErrorMessage = '';
        const params = new HttpParams()
          .set('request', this.request);
        this.workService.getByAnyRequest(params).subscribe(
          response => { this.works = response; this.worksResponseMessage = response.message; },
      error => this.worksErrorMessage = error.error.message);
  }

  deleteWorkEntry(deleteWorkRequest: Work): void {
    this.worksResponseMessage = '';
    this.worksErrorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteWorkRequest.id));
    this.workService.deleteWorkEntry(params).subscribe(data => {
      this.worksResponseMessage = data.message;
    }, error => this.worksErrorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToWorkCreate(): void {
    this.router.navigate(['work/create']);
  }

  navigateToWorkUpdate(id: number): void {
    this.router.navigate(['updateWork/' + id]);
  }

  openModalForCar(template: TemplateRef<any>, car: Car): void {
    this.modalRefForCar = this.modalService.show(template);
    this.car = car;
  }

  openModalCarService(template: TemplateRef<any>, carService: CarService): void {
    this.modalRefForCarService = this.modalService.show(template);
    this.carService = carService;
    this.address = carService.address;
    this.workTime = carService.workTime;
  }

  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }

  openModalForAddress(template: TemplateRef<any>): void {
    this.modalRefForAddress = this.modalService.show(template);
  }

  openModalForWorkTime(template: TemplateRef<any>): void {
    this.modalRefForWorkTime = this.modalService.show(template);
  }
}

