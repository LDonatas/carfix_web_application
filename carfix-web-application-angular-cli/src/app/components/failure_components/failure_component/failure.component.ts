import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Failure} from '../../../entities/failure_entity/failure';
import {User} from '../../../entities/user_entity/user';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';

@Component({
  selector: 'app-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.css']
})
export class FailureComponent implements OnInit {

  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  failures: Failure[];
  user: User;
  loggedInUser: User;
  roles: string[];
  failuresResponseMessage = '';
  failuresErrorMessage = '';
  request: string;
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  modalRefForDelete: BsModalRef;
  confirmText = 'Update';

  constructor(private tokenService: TokenService,
              private failureService: ServiceFailure,
              private router: Router,
              private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      this.loggedInUser = this.tokenService.getUser();
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.failureService.getAllFailureEntries().subscribe(
          failuresResponse => this.failures = failuresResponse,
          error => this.failuresErrorMessage = error.error.message);
      }
      if (this.showUserBoard) {
        this.failureService.getFailureWithUserId(this.loggedInUser.id).subscribe(
          failuresResponse => {
            this.failures = failuresResponse;
            this.failuresResponseMessage = failuresResponse.message;
          },
          error => this.failuresErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  searchByAnyRequest(): void {
    this.failuresResponseMessage = '';
    this.failuresErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.failureService.getByAnyRequest(params).subscribe(failuresResponse => {
      this.failuresResponseMessage = failuresResponse.message;
      this.failures = failuresResponse;
    }, error => this.failuresErrorMessage = error.error.message);
  }

  deleteFailureEntry(deleteFailureRequest: Failure): void {
    this.failuresResponseMessage = '';
    this.failuresErrorMessage = '';
    const params = new HttpParams()
      .set('id', String(deleteFailureRequest.id));
    this.failureService.deleteFailureEntry(params).subscribe(data => {
      this.failuresResponseMessage = data.message;
    }, error => this.failuresErrorMessage = error.error.message);
    this.modalRefForDelete.hide();
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToFailureCreate(): void {
    this.router.navigate(['failure/create']);
  }

  navigateToUpdateFailure(id: number): void {
    this.router.navigate(['updateFailure/' + id]);
  }

  openModalFailure(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }

  openModalUser(template: TemplateRef<any>, user: User): void {
    this.modalRef2 = this.modalService.show(template);
    this.user = user;
  }
  openModalForDelete(template: TemplateRef<any>): void {
    this.modalRefForDelete = this.modalService.show(template);
  }
}
