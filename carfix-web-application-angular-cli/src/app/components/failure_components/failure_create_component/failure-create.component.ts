import {Component, OnInit, TemplateRef} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {Router} from '@angular/router';
import {Failure} from '../../../entities/failure_entity/failure';
import {User} from '../../../entities/user_entity/user';
import {UserService} from '../../../services/entity_services/user_entity_service/user.service';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';

@Component({
  selector: 'app-failure-create',
  templateUrl: './failure-create.component.html',
  styleUrls: ['./failure-create.component.css']
})
export class FailureCreateComponent implements OnInit {

  failure: Failure = new Failure();
  users: User[];
  user: User;
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  createFailureForm: FormGroup;
  submitted = false;
  isFailed = false;
  userIsSelected = false;
  createResponseMessage = '';
  createErrorMessage = '';
  userResponseMessage = '';
  userErrorMessage = '';
  request: string;
  modalRef: BsModalRef;

  constructor(private tokenService: TokenService,
              private userService: UserService,
              private failureService: ServiceFailure,
              private router: Router,
              private modalService: BsModalService, ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      this.user = this.tokenService.getUser();
      this.createFailureForm = this.failureService.getFailuresFormToCreate();
      if (this.showAdminBoard || this.showModeratorBoard) {
        this.userService.getAllUserEntries().subscribe(
          usersResponse => this.users = usersResponse,
          error => this.userErrorMessage = error.error.message);
      }
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.createFailureForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.createFailureForm.invalid) {
      return;
    } else {
      this.failureService.bindValues(this.failure, this.createFailureForm);
      this.create();
      this.isFailed = false;
      this.createErrorMessage = '';
      this.navigateToFailuresPage();
    }
  }

  create(): void {
    this.failure.user = this.user;
    this.failureService.createFailureEntry(this.failure)
      .subscribe(data => this.createResponseMessage = data.message,
        error => {
          this.createErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.failure = new Failure();
  }

  navigateToFailuresPage(): void {
    setTimeout(() => {
      this.router.navigate(['failures'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  searchUserByAnyRequest(): void {
    /*this.userResponseMessage = '';
    this.userErrorMessage = '';
    const params = new HttpParams()
      .set('request', this.request);
    this.carService.getByAnyRequest(params).subscribe(data => {
      this.cars = data;
      this.userResponseMessage = data.message;
    }, error => this.userErrorMessage = error.error.message);*/
  }

  selectUser(selectedUser: User): void {
    this.user = selectedUser;
    this.userIsSelected = true;
    this.modalRef.hide();
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['failures']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, {class: 'modal-xl'});
  }
}

