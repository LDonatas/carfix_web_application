import {Component, OnInit, TemplateRef} from '@angular/core';
import {Failure} from '../../../entities/failure_entity/failure';
import {User} from '../../../entities/user_entity/user';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {TokenService} from '../../../services/token/token.service';
import {ServiceFailure} from '../../../services/entity_services/failure_entity_service/service-failure.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../services/entity_services/user_entity_service/user.service';

@Component({
  selector: 'app-failure-update',
  templateUrl: './failure-update.component.html',
  styleUrls: ['./failure-update.component.css']
})
export class FailureUpdateComponent implements OnInit {

  failureID: number;
  failure: Failure;
  user: User;
  roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  updateFailureForm: FormGroup;
  submitted = false;
  isFailed = false;
  failureResponseMessage = '';
  failureErrorMessage = '';
  updateResponseMessage = '';
  updateErrorMessage = '';
  userErrorMessage = '';
  request: string;
  modalRef: BsModalRef;
  modalRefUser: BsModalRef;

  constructor(private tokenService: TokenService,
              private userService: UserService,
              private failureService: ServiceFailure,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: BsModalService, ) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.showUserBoard = this.roles.includes('ROLE_USER');
      this.failureID = +this.activatedRoute.snapshot.paramMap.get('id');
      this.updateFailureForm = this.failureService.getFailureFormToUpdate();
      this.failureService.getFailureWithId(this.failureID).subscribe(
        failureResponse => {
          this.user = failureResponse.user;
          this.failure = failureResponse;
          this.failureResponseMessage = failureResponse.message;
        },
        error => this.failureErrorMessage = error.error.message);
    } else {
      this.navigateToLoginPage();
    }
  }

  get f(): { [p: string]: AbstractControl } {
    return this.updateFailureForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.updateFailureForm.invalid) {
      return;
    } else {
      this.failureService.bindValues(this.failure, this.updateFailureForm);
      this.update();
      this.isFailed = false;
      this.updateErrorMessage = '';
      this.navigateToFailuresPage();
    }
  }

  update(): void {
    this.failure.user = this.user;
    this.failureService.updateFailureEntry(this.failure)
      .subscribe(data => this.updateResponseMessage = data.message,
        error => {
          this.updateErrorMessage = error.error.message;
          this.isFailed = true;
        });
    this.failure = new Failure();
  }

  navigateToFailuresPage(): void {
    setTimeout(() => {
      this.router.navigate(['failures'])
        .then(() => {
          window.location.reload();
        });
    }, 3000);
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }

  navigateToLoginPage(): void {
    this.router.navigate(['signIn']);
  }

  back(): void {
    this.router.navigate(['failures']);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template);
  }
  openModalUser(template: TemplateRef<any>): void {
    this.modalRefUser = this.modalService.show(template);
  }
}
