import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FailureUpdateComponent } from './failure-update.component';

describe('FailureUpdateComponent', () => {
  let component: FailureUpdateComponent;
  let fixture: ComponentFixture<FailureUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FailureUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FailureUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
