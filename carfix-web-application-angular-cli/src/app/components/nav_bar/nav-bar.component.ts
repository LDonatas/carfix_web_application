import {Component, OnInit} from '@angular/core';
import {TokenService} from '../../services/token/token.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  profile: string;
  showBurger = true;
  moreClicked = false;

  showCars = false;
  showCarCreate = false;
  showCarAll = false;

  showCarService = false;
  showCarServiceCreate = false;
  showCarServiceAll = false;

  showDetails = false;
  showDetailsCreate = false;
  showDetailsAll = false;

  showSchedules = false;
  showSchedulesCreate = false;
  showSchedulesAll = false;

  showWorks = false;
  showWorksCreate = false;
  showWorksAll = false;

  constructor(private tokenService: TokenService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenService.getToken();
    if (this.isLoggedIn) {
      this.roles = this.tokenService.getUser().role;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');
      this.profile = this.tokenService.getUser().username;
    }
  }

  logout(): void {
    this.tokenService.signOut();
    window.location.reload();
  }

  enableShowCar(): void {
    this.showCarService = false;
    this.showDetails = false;
    this.showSchedules = false;
    this.showWorks = false;
    this.showCars = !this.showCars;
    this.showCarCreate = false;
    this.showCarAll = false;
  }
  enableCarCreate(): void {
    this.showCarCreate = true;
    this.showCarAll = false;
  }
  enableCarAll(): void {
    this.showCarAll = true;
    this.showCarCreate = false;
  }


  enableShowCarService(): void {
    this.showCars = false;
    this.showDetails = false;
    this.showSchedules = false;
    this.showWorks = false;
    this.showCarService = !this.showCars;
    this.showCarServiceCreate = false;
    this.showCarServiceAll = false;
  }
  enableCarServiceCreate(): void {
    this.showCarServiceCreate = true;
    this.showCarServiceAll = false;
  }
  enableCarServiceAll(): void {
    this.showCarServiceAll = true;
    this.showCarServiceCreate = false;
  }

  enableShowDetails(): void {
    this.showCars = false;
    this.showCarService = false;
    this.showSchedules = false;
    this.showWorks = false;
    this.showDetails = !this.showDetails;
    this.showDetailsCreate = false;
    this.showDetailsAll = false;
  }
  enableDetailCreate(): void {
    this.showDetailsCreate = true;
    this.showDetailsAll = false;
  }
  enableDetailAll(): void {
    this.showDetailsAll = true;
    this.showDetailsCreate = false;
  }

  enableShowSchedules(): void {
    this.showCars = false;
    this.showCarService = false;
    this.showDetails = false;
    this.showWorks = false;
    this.showSchedules = !this.showSchedules;
    this.showSchedulesCreate = false;
    this.showSchedulesAll = false;
  }
  enableSchedulesCreate(): void {
    this.showSchedulesCreate = true;
    this.showSchedulesAll = false;
  }
  enableSchedulesAll(): void {
    this.showSchedulesAll = true;
    this.showSchedulesCreate = false;
  }

  enableShowWorks(): void {
    this.showCars = false;
    this.showCarService = false;
    this.showDetails = false;
    this.showSchedules = false;
    this.showWorks = !this.showWorks;
    this.showWorksCreate = false;
    this.showWorksAll = false;
  }
  enableWorksCreate(): void {
    this.showWorksCreate = true;
    this.showWorksAll = false;
  }
  enableWorksAll(): void {
    this.showWorksAll = true;
    this.showWorksCreate = false;
  }

  toogleShowLinksInBurger(): void {
    this.showBurger = !this.showBurger;
  }

  openNav(): void {
    this.showCars = false;
    this.showCarService = false;
    this.showDetails = false;
    this.showSchedules = false;
    this.showWorks = false;
    document.getElementById('mySidenav').style.width = '250px';
  }

  closeNav(): void {
    document.getElementById('mySidenav').style.width = '0';
  }
}
