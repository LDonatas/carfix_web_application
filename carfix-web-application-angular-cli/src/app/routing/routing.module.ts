import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UserRegistrationComponent} from '../components/user/user_registration_component/user-registration.component';
import {HomeComponent} from '../components/home/home.component';
import {ProfileComponent} from '../components/user/profile_component/profile.component';
import {UserLoginComponent} from '../components/user/user_login_component/user-login.component';
import {CarComponent} from '../components/car_components/car_component/car.component';
import {CarUpdateComponent} from '../components/car_components/update_car_component/car-update/car-update.component';
import {ServiceCar} from '../services/entity_services/car_entity_service/service-car.service';
import {CreateCarComponent} from '../components/car_components/create_car_component/create-car.component';
import {CarServicesComponent} from '../components/car_services_components/car_services_component/car-services.component';
import {ServiceCarService} from '../services/entity_services/car_service_entity_service/service-car.service';
import {CarServicesUpdateComponent} from '../components/car_services_components/car_services_update_component/car-services-update.component';
import {CarServicesCreateComponent} from '../components/car_services_components/car_services_create_component/car-services-create.component';
import {DetailComponent} from '../components/detail_components/detail_component/detail.component';
import {DetailCreateComponent} from '../components/detail_components/detail_create_component/detail-create.component';
import {ServiceDetail} from '../services/entity_services/detail_entity_service/service-detail.service';
import {DetailUpdateComponent} from '../components/detail_components/detail_update_component/detail-update.component';
import {FailureCreateComponent} from '../components/failure_components/failure_create_component/failure-create.component';
import {ServiceFailure} from '../services/entity_services/failure_entity_service/service-failure.service';
import {FailureUpdateComponent} from '../components/failure_components/failure_update_component/failure-update.component';
import {FailureComponent} from '../components/failure_components/failure_component/failure.component';
import {ScheduleComponent} from '../components/schedule_components/schedule_component/schedule.component';
import {ScheduleCreateComponent} from '../components/schedule_components/schedule_create_component/schedule-create.component';
import {ServiceSchedule} from '../services/entity_services/schedule_entity_service/service-schedule.service';
import {ScheduleUpdateComponent} from '../components/schedule_components/schedule_update_component/schedule-update.component';
import {WorkComponent} from "../components/works_components/work_component/work.component";
import {ServiceWork} from "../services/entity_services/works_entity_service/service-work.service";
import {WorkUpdateComponent} from "../components/works_components/work_update_component/work-update.component";
import {WorkCreateComponent} from "../components/works_components/work_create_component/work-create.component";
import {UserCarComponent} from "../components/user_car_components/user-car/user-car.component";
import {RegistrationComponent} from "../components/registration_components/registration_component/registration.component";
import {RegistrationCreateComponent} from "../components/registration_components/registration-create/registration-create.component";
import {ServiceRegistration} from "../services/entity_services/registration_entity_service/service-registration.service";
import {RegistrationUpdateComponent} from "../components/registration_components/registration-update/registration-update.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'signIn', component: UserLoginComponent},
  {path: 'registration', component: UserRegistrationComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'cars', component: CarComponent},
  {path: 'carServices', component: CarServicesComponent},
  {path: 'details', component: DetailComponent},
  {path: 'failures', component: FailureComponent},
  {path: 'schedules', component: ScheduleComponent},
  {path: 'works', component: WorkComponent},
  {path: 'user_cars', component: UserCarComponent},
  {path: 'registrations', component: RegistrationComponent},
];

const childRoutes: Routes = [
  {path: 'cars/create', component: CreateCarComponent},
  {path: 'updateCar/:id', canActivate: [ServiceCar], component: CarUpdateComponent},
  {path: 'carService/create', component: CarServicesCreateComponent},
  {path: 'updateCarService/:id', canActivate: [ServiceCarService], component: CarServicesUpdateComponent},
  {path: 'detail/create', component: DetailCreateComponent},
  {path: 'updateDetail/:id', canActivate: [ServiceDetail], component: DetailUpdateComponent},
  {path: 'failure/create', component: FailureCreateComponent},
  {path: 'updateFailure/:id', canActivate: [ServiceFailure], component: FailureUpdateComponent},
  {path: 'schedule/create', component: ScheduleCreateComponent},
  {path: 'updateSchedule/:id', canActivate: [ServiceSchedule], component: ScheduleUpdateComponent},
  {path: 'work/create', component: WorkCreateComponent},
  {path: 'updateWork/:id', canActivate: [ServiceWork], component: WorkUpdateComponent},
  {path: 'registration/create', component: RegistrationCreateComponent},
  {path: 'updateRegistration/:id', canActivate: [ServiceRegistration], component: RegistrationUpdateComponent},
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    RouterModule.forChild(childRoutes),
    CommonModule
  ]
})
export class RoutingModule {
}
