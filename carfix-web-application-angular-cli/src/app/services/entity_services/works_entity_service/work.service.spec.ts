import { TestBed } from '@angular/core/testing';

import { ServiceWork } from './service-work.service';

describe('WorkService', () => {
  let service: ServiceWork;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceWork);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
