import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Work} from '../../../entities/works_entity/work';

@Injectable({
  providedIn: 'root'
})
export class ServiceWork {

  API = 'http://localhost:8080/api/carfix/works';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  WORK_TYPE = 'Steering wheel|Wheel|Engine|Electronics|Diagnostics';
  DOUBLE_NUMBER = '\\d+.\\d+';

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createWorkEntry(createWorkRequest: Work): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createWorkRequest, this.httpOptions);
  }

  updateWorkEntry(updateWorkRequest: Work): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateWorkRequest, this.httpOptions);
  }

  deleteWorkEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getWorkWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getWorkById/` + id, this.httpOptions);
  }

  getAllWorkEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/works'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

  getWorkFormToCreate(): FormGroup {
    const workFormForCreate = this.formBuilder.group({
      workType: ['', [Validators.required, Validators.pattern(this.WORK_TYPE)]],
      workName: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern(this.DOUBLE_NUMBER)]]
    });
    return workFormForCreate;
  }

  getWorkFormToUpdate(): FormGroup {
    const workFormForUpdate = this.formBuilder.group({
      workType: ['', [Validators.pattern(this.WORK_TYPE)]],
      workName: [''],
      price: ['', [Validators.pattern(this.DOUBLE_NUMBER)]]
    });
    return workFormForUpdate;
  }

  bindValues(work: Work, workForm: FormGroup): void {

    const workTypeForm = workForm.get('workType').value;
    const workNameForm = workForm.get('workName').value;
    const workPrice = workForm.get('price').value;

    if (workTypeForm !== '') {
      work.workType = workTypeForm;
    }
    if (workNameForm !== '') {
      work.workName = workNameForm;
    }
    if (workPrice !== '') {
      work.price = workPrice;
    }
  }
}
