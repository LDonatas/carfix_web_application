import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs'
import {Schedule} from "../../../entities/schedule_entity/schedule";

@Injectable({
  providedIn: 'root'
})
export class ServiceSchedule {

  API = 'http://localhost:8080/api/carfix/schedules';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private date: Date;
  private year: number;
  private month: string;
  private day: string;

  constructor(private http: HttpClient,
              private router: Router, ) {
  }

  createScheduleEntry(createSchedule: Schedule): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createSchedule, this.httpOptions);
  }

  updateScheduleEntry(updatedSchedule: Schedule): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updatedSchedule, this.httpOptions);
  }

  deleteScheduleEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getScheduleWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getScheduleById/` + id, this.httpOptions);
  }

  getAllScheduleEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/schedules'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

  bindValues(scheduleRequest: Schedule, selectedDate: string, selectedTime: string): void {
    if (selectedDate !== '') {
      scheduleRequest.date = this.convertDate(selectedDate);
    }
    if (selectedTime !== '') {
      scheduleRequest.time = this.convertTime(selectedTime);
    }
  }

  convertDate(selectedDate: string): string {
    this.date = new Date(selectedDate);
    this.month = '' + (this.date.getMonth() + 1);
    this.day = '' + this.date.getDate();
    this.year = this.date.getFullYear();
    if (this.month.length < 2) {
      this.month = '0' + this.month;
    }
    if (this.day.length < 2) {
      this.day = '0' + this.day;
    }
    return [this.year, this.month, this.day].join('-');
  }

  convertTime(selectedTime: string): string {
    const time = new Date(selectedTime);
    const sh = (time.getHours() < 10 ? '0' : '') + time.getHours();
    const sm = (time.getMinutes() < 10 ? '0' : '') + time.getMinutes()
    return sh + ':' + sm;
  }
}
