import { TestBed } from '@angular/core/testing';

import { ServiceSchedule } from './service-schedule.service';

describe('ScheduleService', () => {
  let service: ServiceSchedule;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceSchedule);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
