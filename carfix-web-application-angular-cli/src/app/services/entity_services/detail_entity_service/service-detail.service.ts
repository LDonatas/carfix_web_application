import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Detail} from '../../../entities/detail_entity/detail';

@Injectable({
  providedIn: 'root'
})
export class ServiceDetail {

  API = 'http://localhost:8080/api/carfix/details';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  DOUBLE_NUMBER = '\\d+.\\d+';
  CITY_OR_COUNTRY = '[A-Z]+|[a-z]+|[A-z]+';

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createDetailEntry(createDetailRequest: Detail): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createDetailRequest, this.httpOptions);
  }

  updateDetailEntry(updateDetailRequest: Detail): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateDetailRequest, this.httpOptions);
  }

  deleteDetailEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getDetailWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getDetailById/` + id, this.httpOptions);
  }

  getAllDetailEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/details'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

  getDetailsFormToCreate(): FormGroup {
    const detailsFormForCreate = this.formBuilder.group({
      detailType: ['', Validators.required],
      detailName: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern(this.DOUBLE_NUMBER)]],
      originCountry: ['', [Validators.required, Validators.pattern(this.CITY_OR_COUNTRY)]]
    });
    return detailsFormForCreate;
  }

  getDetailFormToUpdate(): FormGroup {
    const detailsFormForUpdate = this.formBuilder.group({
      detailType: [''],
      detailName: [''],
      price: ['', [Validators.pattern(this.DOUBLE_NUMBER)]],
      originCountry: ['', [Validators.pattern(this.CITY_OR_COUNTRY)]]
    });
    return detailsFormForUpdate;
  }

  bindValues(detail: Detail, detailForm: FormGroup): void {
    const detailTypeForm = detailForm.get('detailType').value;
    const detailNameForm = detailForm.get('detailName').value;
    const priceForm = detailForm.get('price').value;
    const originCountryForm = detailForm.get('originCountry').value;

    if (detailTypeForm !== '') {
      detail.detailType = detailTypeForm;
    }
    if (detailNameForm !== '') {
      detail.detailName = detailNameForm;
    }
    if (priceForm !== '') {
      detail.price = priceForm;
    }
    if (originCountryForm !== '') {
      detail.originCountry = originCountryForm;
    }
  }

}
