import { TestBed } from '@angular/core/testing';

import { ServiceFailure } from './service-failure.service';

describe('FailureService', () => {
  let service: ServiceFailure;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceFailure);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
