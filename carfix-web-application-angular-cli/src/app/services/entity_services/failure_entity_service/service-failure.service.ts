import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Failure} from '../../../entities/failure_entity/failure';

@Injectable({
  providedIn: 'root'
})
export class ServiceFailure {

  API = 'http://localhost:8080/api/carfix/failures';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createFailureEntry(createFailureRequest: Failure): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createFailureRequest, this.httpOptions);
  }

  updateFailureEntry(updateFailureRequest: Failure): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateFailureRequest, this.httpOptions);
  }

  deleteFailureEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getFailureWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getFailureById/` + id, this.httpOptions);
  }

  getFailureWithUserId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getFailuresByUserId/` + id, this.httpOptions);
  }

  getAllFailureEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/failures'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

  getFailuresFormToCreate(): FormGroup {
    const failuresFormForCreate = this.formBuilder.group({
      failureLocation: ['', Validators.required],
      failureType: ['', [Validators.required]],
      failureDescription: ['', [Validators.required, Validators.maxLength(10000)]]
    });
    return failuresFormForCreate;
  }

  getFailureFormToUpdate(): FormGroup {
    const failuresFormForUpdate = this.formBuilder.group({
      failureLocation: [''],
      failureType: [''],
      failureDescription: ['', [Validators.maxLength(10000)]]
    });
    return failuresFormForUpdate;
  }

  bindValues(failure: Failure, failureForm: FormGroup): void {
    const failureLocationForm = failureForm.get('failureLocation').value;
    const failureTypeForm = failureForm.get('failureType').value;
    const failureDescriptionForm = failureForm.get('failureDescription').value;

    if (failureLocationForm !== '') {
      failure.failureLocation = failureLocationForm;
    }
    if (failureTypeForm !== '') {
      failure.failureType = failureTypeForm;
    }
    if (failureDescriptionForm !== '') {
      failure.failureDescription = failureDescriptionForm;
    }
  }
}
