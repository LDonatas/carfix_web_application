import { TestBed } from '@angular/core/testing';

import { ServiceUserCar } from './service-user-car.service';

describe('ServiceUserCarService', () => {
  let service: ServiceUserCar;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceUserCar);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
