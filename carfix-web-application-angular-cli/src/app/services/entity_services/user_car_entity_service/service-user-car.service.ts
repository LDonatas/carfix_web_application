import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {UserCarRequest} from '../../../entities/user_car_entity/user-car-request';

@Injectable({
  providedIn: 'root'
})
export class ServiceUserCar {

  API = 'http://localhost:8080/api/carfix/user_cars';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  updateUserCarEntry(updateUserCarRequest: UserCarRequest): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateUserCarRequest, this.httpOptions);
  }

  deleteUserCarEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getUserCarsWithUserId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getUserCarsByUserId/` + id, this.httpOptions);
  }

  getAllUsersCarEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/user_cars'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }
}
