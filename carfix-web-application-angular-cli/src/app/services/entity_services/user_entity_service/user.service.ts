import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  API = 'http://localhost:8080/api/carfix/users';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  getAllUserEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getUserWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getUserById/` + id, this.httpOptions);
  }

  getUserWithUsername(username: string): Observable<any> {
    return this.http.get(`${this.API}` + `/getUserByUsername/` + username, this.httpOptions);
  }

  getUserWithEmail(email: string): Observable<any> {
    return this.http.get(`${this.API}` + `/getUserByEmail/` + email, this.httpOptions);
  }

  getUserWithPhoneNumber(phonenumber: string): Observable<any> {
    return this.http.get(`${this.API}` + `/getUserByPhoneNumber/` + phonenumber, this.httpOptions);
  }

}
