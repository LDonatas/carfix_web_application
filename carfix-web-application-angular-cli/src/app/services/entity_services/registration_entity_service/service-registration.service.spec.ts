import { TestBed } from '@angular/core/testing';

import { ServiceRegistration } from './service-registration.service';

describe('ServiceRegistrationService', () => {
  let service: ServiceRegistration;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceRegistration);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
