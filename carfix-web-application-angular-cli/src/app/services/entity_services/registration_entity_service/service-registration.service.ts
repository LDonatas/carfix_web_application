import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {RegistrationRequest} from '../../../entities/registration_entity/registration-request';

@Injectable({
  providedIn: 'root'
})
export class ServiceRegistration {

  API = 'http://localhost:8080/api/carfix/registration';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createRegistrationEntry(createRegistrationRequest: RegistrationRequest): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createRegistrationRequest, this.httpOptions);
  }

  updateRegistrationEntry(updateRegistrationRequest: RegistrationRequest): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateRegistrationRequest, this.httpOptions);
  }

  deleteRegistrationEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getRegistrationWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getRegistrationById/` + id, this.httpOptions);
  }

  getAllRegistrationEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/registrations'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

}
