import { TestBed } from '@angular/core/testing';

import { ServiceCarService } from './service-car.service';

describe('CarServiceService', () => {
  let service: ServiceCarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceCarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
