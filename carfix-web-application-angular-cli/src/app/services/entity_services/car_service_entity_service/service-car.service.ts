import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {CarService} from '../../../entities/car_service_entity/car-service';
import {Address} from '../../../entities/address_entity/address';
import {WorkTime} from '../../../entities/work_time_entity/work-time';
import {Time} from '../../../entities/work_time_entity/week_entity/time';

@Injectable({
  providedIn: 'root'
})
export class ServiceCarService {

  API = 'http://localhost:8080/api/carfix/carServices';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  MANUFACTURE_YEARS = '[1-2][0-9][0-9][0-9]-[0][0-9]|[1-2][0-9][0-9][0-9]-[1][0-2]';
  ENGINE_DISPLACEMENT = '\\d+.\\d+';
  ENGINE_TYPE = 'diesel|gasoline|electric|gas';
  NUMBER = '\\d+';
  WORK_TIME = '(?:(?:2[0-3])|(?:[01]?[0-9]))(?:\\:[0-5][0-9])?-((?<=-)(?:(?:2[0-3])|(?:[01]?[0-9]))(?:\\:[0-5][0-9])?)|Not working';
  STREET_REGEXP = '([A-z]+ (g.|pl.|pr.))';
  BUILDING_NUMBER = '(\\d+[A-z])|(\\d+[a-z])|\\d+';
  CITY_OR_COUNTRY = '[A-Z]+|[a-z]+|[A-z]+';

  private date: Date;
  private month: string;
  private day: string;

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createCarServiceEntry(createCarServiceRequest: CarService): Observable<any> {
    return this.http.post(`${this.API}` + `/edit/create`, createCarServiceRequest, this.httpOptions);
  }

  updateCarServiceEntry(updateCarServiceRequest: CarService): Observable<any> {
    return this.http.put(`${this.API}` + `/edit/update`, updateCarServiceRequest, this.httpOptions);
  }

  deleteCarServiceEntry(params): Observable<any> {
    return this.http.delete(`${this.API}` + `/edit/delete`, {params});
  }

  getCarServiceWithId(id: number): Observable<any> {
    return this.http.get(`${this.API}` + `/getCarServiceById/` + id, this.httpOptions);
  }

  getAllCarServiceEntries(): Observable<any> {
    return this.http.get(`${this.API}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.API}` + `/getByAny`, {params});
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/carServices'], {state: {error: 'Invalid URL parameter: ' + next.url[1].path}});
      return false;
    }
    return true;
  }

  getCarServiceFormToCreate(): FormGroup {
    const carServiceFormForCreate = this.formBuilder.group({
      serviceName: ['', Validators.required],
      streetName: ['', [Validators.required, Validators.pattern(this.STREET_REGEXP)]],
      buildingNumber: ['', [Validators.required, Validators.pattern(this.BUILDING_NUMBER)]],
      city: ['', [Validators.required, Validators.pattern(this.CITY_OR_COUNTRY)]],
      country: ['', [Validators.required, Validators.pattern(this.CITY_OR_COUNTRY)]],
      postCode: ['', [Validators.required, Validators.pattern(this.NUMBER)]],
      employeesNum: ['', [Validators.required, Validators.pattern(this.NUMBER)]],
      workTimeRegistered: [true, [Validators.required]]
    });
    return carServiceFormForCreate;
  }

  getCarServiceFormToUpdate(): FormGroup {
    const carServiceFormForUpdate = this.formBuilder.group({
      serviceName: [''],
      streetName: ['', [Validators.pattern(this.STREET_REGEXP)]],
      buildingNumber: ['', [Validators.pattern(this.BUILDING_NUMBER)]],
      city: ['', [Validators.pattern(this.CITY_OR_COUNTRY)]],
      country: ['', [Validators.pattern(this.CITY_OR_COUNTRY)]],
      postCode: ['', [Validators.minLength(4), Validators.pattern(this.NUMBER)]],
      employeesNum: ['', [Validators.pattern(this.NUMBER)]],
    });
    return carServiceFormForUpdate;
  }

  bindValues(carService: CarService, address: Address, workTime: WorkTime, time: Time, carServiceForm: FormGroup): void {

    const serviceNameForm = carServiceForm.get('serviceName').value;
    const streetNameForm = carServiceForm.get('streetName').value;
    const buildingNumberForm = carServiceForm.get('buildingNumber').value;
    const cityForm = carServiceForm.get('city').value;
    const countryForm = carServiceForm.get('country').value;
    const postCodeForm = carServiceForm.get('postCode').value;

    const employeesNumForm = carServiceForm.get('employeesNum').value;

    if (serviceNameForm !== '') {
      carService.serviceName = serviceNameForm;
    }
    if (streetNameForm !== '') {
      address.streetName = streetNameForm;
    }
    if (buildingNumberForm !== '') {
      address.buildingNumber = buildingNumberForm;
    }
    if (cityForm !== '') {
      address.city = cityForm;
    }
    if (countryForm !== '') {
      address.country = countryForm;
    }
    if (postCodeForm !== '') {
      address.postCode = postCodeForm;
    }

    if (time.mondayStart !== undefined && time.mondayEnd !== undefined) {
      workTime.monday = this.convertAndGetTime(time.mondayStart, time.mondayEnd);
    }
    if (time.tuesdayStart !== undefined && time.tuesdayEnd !== undefined) {
      workTime.tuesday = this.convertAndGetTime(time.tuesdayStart, time.tuesdayEnd);
    }
    if (time.wednesdayStart !== undefined && time.wednesdayEnd !== undefined) {
      workTime.wednesday = this.convertAndGetTime(time.wednesdayStart, time.wednesdayEnd);
    }
    if (time.thursdayStart !== undefined && time.thursdayEnd !== undefined) {
      workTime.thursday = this.convertAndGetTime(time.thursdayStart, time.thursdayEnd);
    }
    if (time.fridayStart !== undefined && time.fridayEnd !== undefined) {
      workTime.friday = this.convertAndGetTime(time.fridayStart, time.fridayEnd);
    }
    if (time.saturdayStart !== undefined && time.saturdayEnd !== undefined) {
      workTime.saturday = this.convertAndGetTime(time.saturdayStart, time.saturdayEnd);
    }
    if (time.sundayStart !== undefined && time.sundayEnd !== undefined) {
      workTime.sunday = this.convertAndGetTime(time.sundayStart, time.sundayEnd);
    }
    if (employeesNumForm !== '') {
      carService.employeesNum = employeesNumForm;
    }
  }

  convertAndGetTime(start: string, end: string): string {

    const startDate = new Date(start);
    const endDate = new Date(end);

    const sh = (startDate.getHours() < 10 ? '0' : '') + startDate.getHours();
    const sm = (startDate.getMinutes() < 10 ? '0' : '') + startDate.getMinutes();

    const eh = (endDate.getHours() < 10 ? '0' : '') + endDate.getHours();
    const em = (endDate.getMinutes() < 10 ? '0' : '') + endDate.getMinutes();

    return sh + ':' + sm + '-' + eh + ':' + em;

  }
}
