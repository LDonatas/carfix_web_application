import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Car} from '../../../entities/car_entity/car';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ServiceCar implements CanActivate{

  carAPI = 'http://localhost:8080/api/carfix/cars';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  MANUFACTURE_YEARS = '[1-2][0-9][0-9][0-9]-[0][0-9]|[1-2][0-9][0-9][0-9]-[1][0-2]';
  ENGINE_DISPLACEMENT = '\\d+.\\d+';
  ENGINE_TYPE = 'diesel|gasoline|electric|gas';
  ENGINE_POWER = '\\d+';
  private date: Date;
  private month: string;
  private day: string;

  constructor(private http: HttpClient,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  createCarEntry(createCar: Car): Observable<any> {
    return this.http.post(`${this.carAPI}` + `/edit/create`, createCar, this.httpOptions);
  }

  updateCarEntry(updatedCar: Car): Observable<any> {
    return this.http.put(`${this.carAPI}` + `/edit/update`, updatedCar, this.httpOptions);
  }

  deleteCarEntry(params): Observable<any> {
    return this.http.delete(`${this.carAPI}` + `/edit/delete`, {params});
  }

  getCarWithId(id: number): Observable<any> {
    return this.http.get(`${this.carAPI}` + `/getCarById/` + id,  this.httpOptions);
  }

  getAllCarEntries(): Observable<any> {
    return this.http.get(`${this.carAPI}` + `/getAll`, this.httpOptions);
  }

  getByAnyRequest(params): Observable<any> {
    return this.http.get(`${this.carAPI}` + `/getByAny`, {params});
  }



  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean |
    UrlTree {
    const id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      this.router.navigate(['/cars'], { state: { error: 'Invalid URL parameter: ' + next.url[1].path }});
      return false;
    }
    return true;
  }

  getCreateCarForm(): FormGroup {
    const carForm = this.formBuilder.group({
      brandName: ['', Validators.required],
      seriesName: ['', Validators.required],
      manufactureYear: [''],
      engineDisplacement: ['', [Validators.required, Validators.pattern(this.ENGINE_DISPLACEMENT)]],
      engineType: ['', [Validators.required, Validators.pattern(this.ENGINE_TYPE)]],
      enginePower: ['', [Validators.required, Validators.maxLength(4), Validators.pattern(this.ENGINE_POWER)]],
    });
    return carForm;
  }

  getUpdateCarForm(): FormGroup {
    const updateCarForm = this.formBuilder.group({
      brandName: [''],
      seriesName: [''],
      manufactureYear: [''],
      engineDisplacement: ['', [Validators.pattern(this.ENGINE_DISPLACEMENT)]],
      engineType: ['', [Validators.pattern(this.ENGINE_TYPE)]],
      enginePower: ['', [Validators.maxLength(4), Validators.pattern(this.ENGINE_POWER)]],
    });
    return updateCarForm;
  }

  bindValues(car: Car, carForm: FormGroup, selectedDate: string): void {

    const brandNameForm = carForm.get('brandName').value;
    const seriesNameForm = carForm.get('seriesName').value;
    const engineDisplacementForm = carForm.get('engineDisplacement').value;
    const engineTypeForm = carForm.get('engineType').value;
    const enginePowerForm = carForm.get('enginePower').value;

    if (brandNameForm !== '') {
      car.brandName = brandNameForm;
    }
    if (seriesNameForm !== '') {
      car.seriesName = seriesNameForm;
    }
    if (selectedDate !== '') {
      car.manufactureYear = this.convertDate(selectedDate);
    }
    if (engineDisplacementForm !== '') {
      car.engineDisplacement = engineDisplacementForm;
    }
    if (engineTypeForm !== '') {
      car.engineType = engineTypeForm;
    }
    if (enginePowerForm !== '') {
      car.enginePower = enginePowerForm;
    }
  }

  convertDate(date: string): string {
    this.date = new Date(date),
      this.month = ('0' + (this.date.getMonth() + 1)).slice(-2),
      this.day = ('0' + this.date.getDate()).slice(-2);
    return [this.date.getFullYear(), this.month].join('-');
  }
}
