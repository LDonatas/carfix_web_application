import { TestBed } from '@angular/core/testing';

import { ServiceCar } from './service-car.service';

describe('CarService', () => {
  let service: ServiceCar;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceCar);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
