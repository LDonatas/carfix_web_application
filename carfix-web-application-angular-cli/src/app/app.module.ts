import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { NavBarComponent } from './components/nav_bar/nav-bar.component';
import {RoutingModule} from './routing/routing.module';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserRegistrationComponent } from './components/user/user_registration_component/user-registration.component';
import { HomeComponent } from './components/home/home.component';
import {authInterceptorProviders} from './services/interceptor/auth.interceptor';
import { ProfileComponent } from './components/user/profile_component/profile.component';
import { UserLoginComponent } from './components/user/user_login_component/user-login.component';
import { CarComponent } from './components/car_components/car_component/car.component';
import { CarUpdateComponent } from './components/car_components/update_car_component/car-update/car-update.component';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import { CreateCarComponent } from './components/car_components/create_car_component/create-car.component';
import { CarServicesComponent } from './components/car_services_components/car_services_component/car-services.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ModalModule} from 'ngx-bootstrap/modal';
import { CarServicesUpdateComponent } from './components/car_services_components/car_services_update_component/car-services-update.component';
import { CarServicesCreateComponent } from './components/car_services_components/car_services_create_component/car-services-create.component';
import {TimepickerModule} from 'ngx-bootstrap/timepicker';
import { DetailComponent } from './components/detail_components/detail_component/detail.component';
import { DetailCreateComponent } from './components/detail_components/detail_create_component/detail-create.component';
import { DetailUpdateComponent } from './components/detail_components/detail_update_component/detail-update.component';
import { FailureComponent } from './components/failure_components/failure_component/failure.component';
import { FailureUpdateComponent } from './components/failure_components/failure_update_component/failure-update.component';
import {FailureCreateComponent} from './components/failure_components/failure_create_component/failure-create.component';
import { ScheduleComponent } from './components/schedule_components/schedule_component/schedule.component';
import { ScheduleCreateComponent } from './components/schedule_components/schedule_create_component/schedule-create.component';
import { ScheduleUpdateComponent } from './components/schedule_components/schedule_update_component/schedule-update.component';
import { WorkComponent } from './components/works_components/work_component/work.component';
import { WorkCreateComponent } from './components/works_components/work_create_component/work-create.component';
import { WorkUpdateComponent } from './components/works_components/work_update_component/work-update.component';
import { UserCarComponent } from './components/user_car_components/user-car/user-car.component';
import { RegistrationComponent } from './components/registration_components/registration_component/registration.component';
import { RegistrationCreateComponent } from './components/registration_components/registration-create/registration-create.component';
import { RegistrationUpdateComponent } from './components/registration_components/registration-update/registration-update.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    UserRegistrationComponent,
    HomeComponent,
    ProfileComponent,
    UserLoginComponent,
    CarComponent,
    CarUpdateComponent,
    CreateCarComponent,
    CarServicesComponent,
    CarServicesUpdateComponent,
    CarServicesCreateComponent,
    DetailComponent,
    DetailCreateComponent,
    DetailUpdateComponent,
    FailureComponent,
    FailureCreateComponent,
    FailureUpdateComponent,
    ScheduleComponent,
    ScheduleCreateComponent,
    ScheduleUpdateComponent,
    WorkComponent,
    WorkCreateComponent,
    WorkUpdateComponent,
    UserCarComponent,
    RegistrationComponent,
    RegistrationCreateComponent,
    RegistrationUpdateComponent,
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    TimepickerModule.forRoot(),
    BrowserAnimationsModule,
    ConfirmationPopoverModule.forRoot({confirmButtonType: 'danger'}),
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
